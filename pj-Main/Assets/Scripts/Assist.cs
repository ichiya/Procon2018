﻿using UnityEngine;
using UnityEngine.UI;
using static SystemProgram;

public class Assist : MonoBehaviour
{
	public Text assistText1;    // 1人目の移動方向
	public Text assistText2;    // 2人目の移動方向

	private Button assistBtn;   // Assistのスイッチ
	public static UIs it1, it2;     // Image, Textの保管
	private static GameObject field;    // 表示するフィールドの要素
	private static Image image1, image2;// 表示する画像
	public static bool selected = false;// スイッチの状態
	private static int[] validTimes = new int[2] { 4, 4 };   // 画像表示から2ターンたつときえるように
															 // 現在のターンから2ターン後の数値を記憶


	void Start()
	{
		// get component
		assistBtn = GameObject.Find("AssistButton").GetComponent<Button>();
		image1 = GameObject.Find("AssistImage1").GetComponent<Image>();
		image2 = GameObject.Find("AssistImage2").GetComponent<Image>();
		field = GameObject.Find("Assist");

		field.gameObject.SetActive(false);  // 非アクティブに

		it1 = new UIs(image1, assistText1);
		it2 = new UIs(image2, assistText2);
	}// end of Start

	/// アシストモード
	public void AssistCommander()
	{
		selected = !selected;   // 反転
		if (selected)
		{
			assistBtn.image.color = new Color(255, 50, 0);  // 黄色っぽく
			field.gameObject.SetActive(true);
		}
		else
		{
			assistBtn.image.color = Color.white;
			field.gameObject.SetActive(false);
		}
	}// end of AssistCommander

	/// 移動テキストの決定 / 呼び出し
	/// @ param
	///		x, y : 現在地との差
	public static void Assisting(int y, int x)
	{
		// テキストの決定
		string str = (x == 1) ? "R-" : (x == 0) ? "C-" : "L-";
		str += (y == 1) ? "bottom" : (y == 0) ? "middle" : "top";

		int id = -1;
		var tex = Resources.Load("UI07") as Texture2D;
		if ((id = System.Array.IndexOf(validTimes, Round % 4)) > -1)	// 見つかった場合
		{
			switch (id)
			{
				case 0:
					it1.Img.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
					it1.Txt.text = null;
					break;
				case 1:
					it2.Img.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
					it2.Txt.text = null;
					break;
			}
		}
		switch (Pc4)
		{
			case 0: Changing(it1, str); validTimes[0] = (Round + 2) % 4; break;
			case 1: Changing(it2, str); validTimes[1] = (Round + 2) % 4; break;
		}
	}// end of Assisting

	/// 移動方向に合わせてテキスト, 画像を変更する
	/// @ param
	///		it  : image, text
	///		str : テキスト内容
	private static void Changing(UIs it, string str)
	{
		// 画像をセット
		var tex = Resources.Load("DirectionImage/" + str) as Texture2D;
		it.Img.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
		it.Txt.text = toJp[str];    // テキストをセット
	}// end of Changing
}// end of Assist
