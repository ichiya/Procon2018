﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.Color;
using static NewFunc;

public class OperatePlayer : MonoBehaviour
{
	// 操作状況
	public static bool[] Enabled { get; set; } = new bool[4];
	
	public static readonly Button[] PlayerNumber = new Button[4];	// ボタンの管理
	public static LinkedList<int> pNumList = new LinkedList<int>(); // 操作順の履歴


	void Start()
	{
		for (int i = 0; i < 4; i++)
			PlayerNumber[i] = GameObject.Find($"BtnPlayer{i}").GetComponent<Button>();
	}// end of Start

	 /// クリックされた番号のプレイヤーをカレントにする
	public void ChangeOrder()
	{
		// 変更したいプレイヤー番号にそろえる
		int dif = int.Parse(name.Substring(9));
		ButtonClick.SetPlayerCount(dif);
		
		SystemProgram.Apply();
	}// end of ChangeOrder

	/// 操作したプレイヤー番号のボタンの色を変える / 印をつける
	/// @ param
	///		n : プレイヤー番号
	public static void FinishedOperating(int n)
	{
		PlayerNumber[n].image.color = yellow;
		Enabled[n] = true;
	}// end of FinishedOperating

	/// ボタンをすべて白にする
	public static void ClearColor()
	{
		for (int i = 0; i < 4; i++)
		{
			PlayerNumber[i].image.color = white;
			Enabled[i] = false;
		}
	}// end of ClearColor

	/// 一回前に戻る
	public static void WhenCalledOnUndo()
	{
		var pop = MyStack.PopLog(ref pNumList);
		Enabled[pop] = false;                   // 前回操作した番号を未操作状態に
		PlayerNumber[pop].image.color = white;

		if (SystemProgram.Round % 4 == 3)       // ターンを戻るとき
		{
			for (int i = 0; i < 4; i++)
				if (i != pop)       // 前回操作した番号以外を操作済みに
					FinishedOperating(i);
		}
	}// end of WhenCalledOnUndo

}// end of OperatePlayer
