﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Process = System.Diagnostics.Process;
using UnityEngine.UI;
using System;

public class StartUp : MonoBehaviour
{
	private Image img;
	public static Color magenta, cyan, red, blue;
	public static bool colors = true;
	public static bool backed = false;


	private void Start()
	{
		// 初期カラーをセット
		ButtonClick.magenta = Color.magenta;
		ButtonClick.cyan = Color.cyan;
		ButtonClick.red = Color.red;
		ButtonClick.blue = Color.blue;
	}// end of Start

	/// 自分のチームの色を変更する
	public void OnChangeMyColor()
	{
		colors = !colors;
		img = GameObject.Find("StartColor").GetComponent<Button>().image;
		if (!colors) img.color = Color.blue;
		else img.color = Color.red;

		Extensions.Swap(ref ButtonClick.magenta, ref ButtonClick.cyan);
		Extensions.Swap(ref ButtonClick.red, ref ButtonClick.blue);
	}// end of OnChangeMyColor

	public void OnBackUp()
	{
		backed = true;
		SceneManager.LoadScene("SimulationSystem");
	}

	/// シーン切り替え
	public void OnStartUp()
		=> SceneManager.LoadScene("SimulationSystem");
	// end of OnStartUp

	/// QRコードを読む
	public void OnLaunchHTML()
	{
		string basePath = Application.dataPath;
		var p = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

		// Projectの文字列からみて初めにくる "/" までを basePathに記憶
		basePath = basePath.Substring(0, basePath.LastIndexOf("/", basePath.IndexOf("pj")));

		var htmlPath = $"{basePath}/Instascan/docs/Browser/index_Browser.html";    // HTML
		Process.Start(htmlPath);    // HTMLの起動

		// どのPCかを判断
		string filePath =
			(p.Contains("Kitakaze")) ? @"C:\Users\Kitakaze\Downloads\data.csv" :	// kitakaze pc
			(p.Contains("kazuya")) ? @"C:\Users\kazuya\Downloads\data.csv" :		// my pc
											  @"C:\Users\Student\Downloads\data.csv";	// student pc

		// data.csvが存在するなら削除
		if (System.IO.File.Exists(filePath))
			System.IO.File.Delete(filePath);
	}// end of OnLaunchHTML
}// end of StartUp
