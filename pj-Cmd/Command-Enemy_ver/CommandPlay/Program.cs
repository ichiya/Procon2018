﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

/*		<Memo List>
	マスの重み：1.0　0.7　0.4 
*/
namespace CommandPlay
{
	class Program
	{
		public static int[,] value; // 得点
		public static int wid, hei; // Trout's Size

		public static int myPx1, myPy1, myPx2, myPy2,   // Ally
						  enPx1, enPy1, enPx2, enPy2;   // Enemy

		public static double[,] firstWeight;  // 重み付けした得点 (for first  Player
		public static double[,] secondWeight;  // 重み付けした得点 (for second Player
		public static int[,] player;    // マスの取得状況
		public static bool[,] board;    // マス囲み

		static void Main(string[] args)
		{
			Game();

			Console.WriteLine($"\nPress Any Key to Exit ...");
			Console.ReadKey();
		}// Main()_end

		/// <summary>
		/// Main System
		/// </summary>
		static void Game()
		{
			OpenFile();         // ファイル読み込み
			Initialization();   // 初期化

			int inTurn = 0;
			input:
			Console.Write("What Turns? -> ");

			try { inTurn = int.Parse(Console.ReadLine()); }     // Input Turn
			catch { Console.WriteLine($" Error. Please Retype.\n"); goto input; }    // Not int -> Retype

			int turn = 1, h;    // for loop
			while (turn <= inTurn)
			{
				// Copy 'val' to 'Clone'
				Array.Copy(value, firstWeight, value.Length);
				Array.Copy(value, secondWeight, value.Length);

				GiveWeight(myPy1, myPx1, firstWeight); GiveWeight(myPy2, myPx2, secondWeight); //Weighting

				SearchRoute.Search();     // Search
				ShowPlayer();
				Console.WriteLine($"Score  ->  Ally  : {Calculation(1)}");    // Show Score
				Console.WriteLine($"           Enemy : {Calculation(-1)}  point.");

				Console.Write($"\n{turn} Trun / {inTurn} Truns.");

				h = 0;
				while (h++ < 4) // 4 Player
				{
					loop:
					Console.Write($"\n\nNo.{h}'s Player Location. ->  ");
					var ch = Console.ReadLine().ToArray();  // Input Player Location
					int flag = 0;  // Judgment

					int rlx, rly;
					try
					{
						rlx = (char.IsUpper(ch[0]) ? ch[0] + 32 - 'a' : ch[0] - 'a');   // 大文字なら小文字にして-'a'
						rly = ch[1] - '0';
					}
					catch { Console.Write(" Error. Please Retype."); goto loop; }

					if (!(rlx >= 0 && rlx <= hei - 1)) flag = -1;
					if (ch.Length > 2 && (rly *= 10 + ch[2] - '0') > wid - 1) flag = -1;
					if (flag == -1) { Console.Write(" Error. Please Retype."); goto loop; }  // flag == -1 -> Retype

					// Can't move -> Retype
					if (CanMove(rlx, rly, (h - 1) * 2, myPx1, myPy1, myPx2, myPy2, enPx1, enPy1, enPx2, enPy2) == -1) goto loop;

					// Confirming
					Console.Write("Really? if it isn't, press N. -> ");
					if (Console.ReadKey().Key == ConsoleKey.N) goto loop;   // If press N -> Retype
					else // If not N -> Move
					{
						switch (h)
						{
							case 1: ToPosition(ref myPx1, ref myPy1, rlx, rly, 0); break;
							case 2: ToPosition(ref myPx2, ref myPy2, rlx, rly, 0); break;
							case 3: ToPosition(ref enPx1, ref enPy1, rlx, rly, 1); break;
							case 4: ToPosition(ref enPx2, ref enPy2, rlx, rly, 1); Console.WriteLine(); break;
						}
					}
				}
				turn++;
			}
		}// Game()_end

		/// <summary>
		/// 移動可能かどうかの判断
		/// </summary>
		/// <param name="rlx"></param>
		/// <param name="rly"></param>
		/// <param name="t"></param>
		/// <param name="P"></param>
		/// <returns></returns>
		static int CanMove(int rlx, int rly, int t, params int[] P)
		{
			// Can't move -> -1
			if (!(rlx - P[t] >= -1 && rlx - P[t] <= 1 && rly - P[t + 1] >= -1 && rly - P[t + 1] <= 1))
			{
				Console.Write(" Error. Cannot move there.");
				return -1;
			}
			// Can move -> 0
			return 0;
		}// CanMove()_end

		/// <summary>
		/// 移動指定マス -> 現在地マス
		/// </summary>
		/// <param name="Px"></param>
		/// <param name="Py"></param>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="check"></para
		static void ToPosition(ref int Px, ref int Py, int x, int y, int check)
		{
			switch (check)
			{
				case 0: // Ally
					if (player[x, y] == PlayerColor.E) player[x, y] = PlayerColor.N;    // Enemy Trout -> Empty
					else (Px, Py, player[x, y]) = (x, y, PlayerColor.A);    // Not Enemy Trout -> Ally
					break;

				case 1: // Enemy
					if (player[x, y] == PlayerColor.A) player[x, y] = PlayerColor.N;    // Ally Trout -> Empty
					else (Px, Py, player[x, y]) = (x, y, PlayerColor.E);    // Not Ally Trout -> Enemy
					break;
			}
		}// ToPosition()_end

		/// <summary>
		/// ファイルデータの読み取り
		/// 
		static void OpenFile()
		{
			try
			{
				string OutPath = null;
				//OutPath = @"C:\Users\Student\Downloads\";                 // School
				OutPath = @"C:\Users\岡村一矢\Downloads\Chrome_Download\";  // Mine

				//string rnPath = @"C:\Users\岡村一矢\Dropbox\program\StudyC#\Procon\rndata.csv";

				using (var sr = new StreamReader(OutPath + "data.csv"))
				{
					string[] cut = sr.ReadToEnd().Split(' ', ':');  // ' ', ':'で区切りを入れる
					var read = new List<string>();

					foreach (var row in cut) read.Add(row);
					read.RemoveAll(x => x == "");   // Remove null

					var arr = read.ConvertAll(x => int.Parse(x)).ToArray();  // string -> int[]
					hei = arr[0]; wid = arr[1];     // width, height
					value = new int[hei, wid];

					int it = 2; // arr 走査用
					for (int i = 0; i < hei; i++)   // arr to val
						for (int j = 0; j < wid; j++, it++)
							value[i, j] = arr[it];

					//エージェントの位置データを取得
					int pos1, pos2, len = arr.Length;

					if ((arr[len - 2] -= 1) < (arr[len - 4] -= 1)) { enPx1 = arr[len - 2]; enPx2 = arr[len - 4]; pos1 = arr[len - 2]; }
					else { enPx1 = arr[len - 4]; enPx2 = arr[len - 2]; pos1 = arr[len - 4]; }

					if ((arr[len - 1] -= 1) < (arr[len - 3] -= 1)) { enPy1 = arr[len - 1]; enPy2 = arr[len - 3]; pos2 = arr[len - 1]; }
					else { enPy1 = arr[len - 3]; enPy2 = arr[len - 1]; pos2 = arr[len - 3]; }

					// 敵の初期位置
					myPx1 = hei - 1 - pos1;
					myPy1 = pos2;
					myPx2 = pos1;
					myPy2 = wid - pos2 - 1;

					OutputIntoFile();   // 各データの書き込み
				}
			}
			catch (Exception e) { Console.WriteLine(e.Message); Environment.Exit(0); }
		}// OpenFile()_end

		/// <summary>
		/// マスの初期化
		/// </summary>
		static void Initialization()
		{
			firstWeight = new double[hei, wid];   // Partitioning
			secondWeight = new double[hei, wid];
			board = new bool[hei, wid];

			player = new int[hei, wid];
			for (int i = 0; i < hei; i++)   // Empty Trout
				for (int j = 0; j < wid; j++)
					player[i, j] = PlayerColor.N;

			// 開始時のマス
			player[myPx1, myPy1] = PlayerColor.A; player[myPx2, myPy2] = PlayerColor.A;
			player[enPx1, enPy1] = PlayerColor.E; player[enPx2, enPy2] = PlayerColor.E;

			ShowBoard(value, "value");
		}// Initialization()_end

		/// <summary>
		/// 各チームの得点
		/// </summary>
		/// <param name="i"></param>
		/// <param name="j"></param>
		/// <param name="Pnum"></param>
		static void Score(int i, int j, int Pnum)
		{
			int[] vx = { 1, 0, -1, 0 }, vy = { 0, 1, 0, -1 };
			if (player[i, j] != Pnum) board[i, j] = false;
			else return;

			for (int x = 0; x < 4; x++)
			{
				if (0 <= i + vx[x] && i + vx[x] < hei && 0 <= j + vy[x] && j + vy[x] < wid)
					if (player[i + vx[x], j + vy[x]] != Pnum && board[i + vx[x], j + vy[x]])
						Score(i + vx[x], j + vy[x], Pnum);
			}
		}// Score()_end

		/// <summary>
		/// 各チームの得点を計算する
		/// </summary>
		/// <param name="Pnum"></param>
		/// <returns></returns>
		static int Calculation(int Pnum)
		{
			for (int i = 0; i < hei; i++)
				for (int j = 0; j < wid; j++)
					board[i, j] = true;

			int ret = 0;
			for (int i = 0; i < hei; i++)
			{
				for (int j = 0; j < wid; j++)
					if (i == 0 || i == hei - 1 || j == 0 || j == wid - 1)
						Score(i, j, Pnum);
			}

			for (int i = 0; i < hei; i++)
			{
				for (int j = 0; j < wid; j++)
				{
					if (board[i, j] && player[i, j] == Pnum)
						ret += value[i, j];
					else if (board[i, j])
						ret += (value[i, j] > 0 ? value[i, j] : value[i, j] * -1);
				}
			}
			return ret;
		}// Calculation()_end

		/// <summary>
		/// 二次元配列を列挙する
		/// </summary>
		static void ShowPlayer()
		{
			Console.WriteLine($"Output Player\t● Ally / ○ Enemy");      // 見出し

			Console.Write("\n     ");
			for (int i = 0; i < wid; i++) Console.Write($"{i,3}");

			Console.Write("\n     ");
			for (int i = 0; i < wid; i++) Console.Write("---");
			Console.WriteLine();

			char CharN = 'A';
			for (int i = 0; i < hei; i++)
			{
				Console.Write($" {CharN++} | ");
				for (int j = 0; j < wid; j++)
					Console.Write(player[i, j] == 1 ? " ●" : player[i, j] == -1 ? " ○" : " －");
				Console.WriteLine();
			}
		}// ShowPlayer()_end

		/// <summary>
		/// 二次元配列を列挙する
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="array"></param>
		/// <param name="name"></param>
		static void ShowBoard<T>(T[,] array, string name)
		{
			Console.WriteLine($"Output {name}");      // 見出し

			Console.Write("\n     ");
			for (int i = 0; i < wid; i++) Console.Write($"{i,5}");

			Console.Write("\n     ");
			for (int i = 0; i < wid; i++) Console.Write("-----");

			char CharN = 'A';
			for (int i = 0; i < hei; i++)
			{
				Console.Write($"\n {CharN++} | ");
				for (int j = 0; j < wid; j++)
					Console.Write($"{array[i, j],5}");
			}
			Console.WriteLine("\n");
		}// ShowBoard()_end

		/// <summary>
		/// Playerから3マス先までの重みを付ける
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		static void GiveWeight(int x, int y, double[,] Board)
		{
			double[] Attach = { 1.0, 0.7, 0.4 };    // 重み（倍率
			int ls = 1;     // ループ用 ＆ 自分を中心とした〇マス先

			while (ls <= 3)
			{
				for (int i = -ls; i < ls; i++)
				{
					/* ← ↑ → ↓ の順 */
					// （それぞれ） 配列外を参照していないなら重みを付与する

					if (!(y + i < 0 || y + i > hei - 1 || x - ls < 0))      // ← Not Error.なら
						Board[y + i, x - ls] = value[y + i, x - ls] * Attach[ls - 1];

					if (!(y - ls < 0 || y - ls > hei - 1 || x + i + 1 < 0 || x + i + 1 > wid - 1))  // ↑ Not Error.なら
						Board[y - ls, x + i + 1] = value[y - ls, x + i + 1] * Attach[ls - 1]; // i + 1

					if (!(y + i + 1 < 0 || y + i + 1 > wid - 1 || y + i + 1 > hei - 1 || x + ls > wid - 1)) // → Not Error.なら
						Board[y + i + 1, x + ls] = value[y + i + 1, x + ls] * Attach[ls - 1]; // i + 1

					if (!(y + ls > hei - 1 || x + i < 0 || x + i > wid - 1))    // ↓ Not Error.なら
						Board[y + ls, x + i] = value[y + ls, x + i] * Attach[ls - 1];
				}
				ls++;
			}
		}// Weight()_end

		/// <summary>
		/// 各データのCSVファイルを出力する
		/// </summary>
		static void OutputIntoFile()
		{
			//string OutPath = null;
			//OutPath = @"C:\Users\Student\Dropbox\program\StudyC#\Procon\TFP_WriteFile\";    // School
			//OutPath = @"C:\Users\岡村一矢\Dropbox\program\StudyC#\Procon\TFP_WriteFile\";   // Mine
			try
			{
				var Square = new StreamWriter("./csv/Square.csv", false);
				var Score = new StreamWriter("./csv/Score.csv", false);
				var Agent = new StreamWriter("./csv/Agent.csv", false);

				Square.Write(hei + "," + wid);      // マスの縦横 書き出し

				foreach (var i in value) Score.Write(i + ",");

				// エージェントの位置 書き出し
				int[,] pos = { { myPx1, myPy1 }, { myPx2, myPy2 }, { enPx1, enPy1 }, { enPx2, enPy2 } };
				foreach (var i in pos) Agent.Write(i + ",");

				Square.Close(); Score.Close(); Agent.Close();   // ファイルを閉じる
			}
			catch (Exception e) { Console.WriteLine(e.Message); Environment.Exit(0); }
		}// OutputIntoFile()_end


		/// <summary>
		/// マスの定義 Ally : 1 / None : 0 / Enemy : -1
		/// </summary>
		struct PlayerColor
		{
			public static int A = 1,    // 味方のマス (Ally
							  N = 0,    // 誰のマスでもない (None
							  E = -1;   // 敵のマス (Enemy
		}// struct.PlayerColor_end

		/// <summary>
		/// class Algorithm
		/// </summary>
		class SearchRoute
		{
			public static int[,,] route = new int[729, 3, 2];   // ルートを記憶
			public static double[] routeScore = new double[729];  // 得点を記憶

			/// <summary>
			/// 3て先までのルートを求める
			/// </summary>
			public static void Search()
			{
				Array.Clear(route, 0, route.Length);    // 0 Clear	（リセットするため
				Array.Clear(routeScore, 0, routeScore.Length);  // 0 Clear

				Console.WriteLine("\n\t/* ==== \"Candidate list\" ==== */\n");  // Separation

				// First
				FindRoute(firstWeight, myPx1, myPy1);
				ShowRoute("1", myPx1, myPy1);

				Console.WriteLine(" -------------------------------\n");   // 区切り（みやすくするため

				// Second
				FindRoute(secondWeight, myPx2, myPy2);
				ShowRoute("2", myPx2, myPy2);

				Console.WriteLine("\t/* ==== \"Candidate list\" end ==== */\n");    // Separation

				//ShowBoard(Clone1, "Clone1");
				//ShowBoard(Clone2, "Clone2");
			}// Search()_end

			/// <summary>
			/// ルートを表示する
			/// </summary>
			/// <param name="name"></param>
			/// <param name="Px"></param>
			/// <param name="Py"></param>
			static void ShowRoute(string name, int Px, int Py)
			{
				int n = FindLoopTimes();
				int nx, ny;

				Console.WriteLine($"Output No.{name} Player\n");
				for (int i = 0; i < n; i++)     // Show Route
				{
					nx = Px + 'A'; ny = Py;
					for (int j = 0; j < route.GetLength(1); j++)
					{
						nx += route[i, j, 0];
						ny += route[i, j, 1];
						Console.Write($" {(char)nx}{ny,-2} ");
					}
					Console.WriteLine($" ->  Score : {routeScore[i]}\n");
				}
			}// ShowRoute()_end

			/// <summary>
			/// ルート候補数を調べる
			/// </summary>
			/// <returns></returns>
			static int FindLoopTimes()
			{
				int r_n = 0;        // 候補数を数える変数
				try
				{
					// 昇順ソート
					int cnt = 0;    // 得点が３番目に大きいルートまでループするための変数
					for (r_n = 0; cnt < 3; r_n++)
					{
						for (int j = routeScore.Length - 1; j > r_n; j--)
						{
							if (routeScore[j] > routeScore[j - 1])
							{
								// 得点入れ替え
								var tempS = routeScore[j];
								routeScore[j] = routeScore[j - 1];
								routeScore[j - 1] = tempS;

								// ルート入れ替え
								for (int k = 0; k < route.GetLength(1); k++)
								{
									for (int l = 0; l < route.GetLength(2); l++)
									{
										var tempR = route[j, k, l];
										route[j, k, l] = route[j - 1, k, l];
										route[j - 1, k, l] = tempR;
									}
								}
							}
						}

						// 得点の候補数を数える
						cnt = 1;
						var n = routeScore[0];
						for (int j = 1; j <= r_n; j++)
						{
							if (n != routeScore[j])
							{
								cnt++;
								n = routeScore[j];
							}
						}
					}
				}
				catch { }
				return r_n;
			}// FindLoopTimes()_end

			/// <summary>
			/// ルートの候補を見つける
			/// </summary>
			/// <param name="Field"></param>
			/// <param name="Px"></param>
			/// <param name="Py"></param>
			public static void FindRoute(double[,] Field, int Px, int Py)
			{
				// i, k, m -> Vertical
				// j, l, n -> Horizontal

				int RouteCnt = 0;   //ルート数
				int oneM = 2, twoM = 2, thiM = 2,    // マスの定義と被らないように
					x, y;

				// Search
				for (int i = -1; i <= 1; i++)
				{
					for (int j = -1; j <= 1; j++)
					{
						x = Px + i; y = Py + j;     // Simple

						// Out Of Range なら continue
						if (x < 0 || x > hei - 1 || y < 0 || y > wid - 1) continue;

						oneM = player[x, y];    // $+1 is (A, N, E)

						// $+1 -> A なら continue
						if (oneM == PlayerColor.A) continue;

						// $+1 -> N なら
						else if (oneM == PlayerColor.N)
						{
							player[x, y] = PlayerColor.A;       // $+1 を A に
							for (int k = -1; k <= 1; k++)
							{
								for (int l = -1; l <= 1; l++)
								{
									// Out Of Range なら continue
									if (x + k < 0 || x + k > hei - 1 || y + l < 0 || y + l > wid - 1) continue;

									twoM = player[x + k, y + l];    // $+2 is (A, N, E)

									// $+2 -> A なら continue
									if (twoM == PlayerColor.A) continue;

									// $+2 -> N なら
									else if (twoM == PlayerColor.N)
									{
										player[x + k, y + l] = PlayerColor.A;     // マスを A に

										for (int m = -1; m <= 1; m++)
										{
											for (int n = -1; n <= 1; n++)
											{
												// Out Of Range なら continue
												if (x + k + m < 0 || x + k + m > hei - 1 || y + l + n < 0 || y + l + n > wid - 1) continue;

												thiM = player[x + k + m, y + l + n];    // $+3 is (A, N, E)

												// $+3 -> A なら continue
												if (thiM == PlayerColor.A) continue;

												// $+3 -> N ならルートを記憶
												else if (thiM == PlayerColor.N)
												{
													// Storing Route
													RouteIn(i, j, k, l, m, n, RouteCnt);

													// Total vlaue
													routeScore[RouteCnt] = Field[x, y] + Field[x + k, y + l] + Field[x + k + m, y + l + n];
													RouteCnt++;
												}

												// $+3 -> E ならルートを記憶
												else if (player[x + k + m, y + l + n] == PlayerColor.E)
												{
													// Storing Route
													RouteIn(i, j, k, l, m, n, RouteCnt);

													// Total value
													routeScore[RouteCnt] = Field[x, y] + Field[x + k, y + l];
													RouteCnt++;
												}
											}
										}
									}
									// $+2 -> E ならルートを記憶
									else if (twoM == PlayerColor.E)
									{
										// Storing Route
										RouteIn(i, j, k, l, 0, 0, RouteCnt);

										// Total value
										routeScore[RouteCnt] = Field[x, y] + Field[x + k, y + l];
										RouteCnt++;
									}
									player[x + k, y + l] = twoM;    // $+2 = towM
								}
							}
						}
						// $+1 -> E なら
						else if (oneM == PlayerColor.E)
						{
							player[x, y] = PlayerColor.A;     // $+1 を A に
							for (int k = -1; k <= 1; k++)
							{
								for (int l = -1; l <= 1; l++)
								{
									// Out Of Range なら continue
									if (x + k < 0 || x + k > hei - 1 || y + l < 0 || y + l > wid - 1) continue;

									twoM = player[x + k, y + l];    // $+2 is (A, N, E)

									// $+2 -> A なら continue
									if (twoM == PlayerColor.A) continue;

									// $+2 -> N ならルートを記憶
									else if (twoM == PlayerColor.N)
									{
										// Storing Route
										RouteIn(i, j, 0, 0, k, l, RouteCnt);

										// Total value
										routeScore[RouteCnt] = Field[x, y] + Field[x + k, y + l];
										RouteCnt++;
									}
									// $+2 -> E ならルートを記憶
									else if (twoM == PlayerColor.E)
									{
										// Storing Route
										RouteIn(i, j, 0, 0, k, l, RouteCnt);

										// Total value
										routeScore[RouteCnt] = Field[x, y];
										RouteCnt++;
									}
								}
							}
						}
						player[x, y] = oneM;    // $+1 = oneM
					}
				}
			}// FindRoute()_end

			/// <summary>
			/// ルート代入
			/// </summary>
			/// <param name="x1"></param>
			/// <param name="y1"></param>
			/// <param name="x2"></param>
			/// <param name="y2"></param>
			/// <param name="x3"></param>
			/// <param name="y3"></param>
			/// <param name="cnt"></param>
			private static void RouteIn(int x1, int y1, int x2, int y2, int x3, int y3, int cnt)
			{
				route[cnt, 0, 0] = x1; route[cnt, 0, 1] = y1;
				route[cnt, 1, 0] = x2; route[cnt, 1, 1] = y2;
				route[cnt, 2, 0] = x3; route[cnt, 2, 1] = y3;
			}// RouteIn()_end

		}// class.SearchRoute_end
	}// class.Program_end
}
