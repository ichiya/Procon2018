// ConsoleApplication8.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>
#include <algorithm>
#include <vector>
#include <ctime>
#include <iomanip>
using namespace std;

int nrand(int n) {
	int ret = 0, s;
    for (int i = 0; i < n; i++)		ret += rand() % 17;
	if ((s = rand() % 10) <= 0.2)	ret *= -1;

	return ret / n;
}

int main() {
    srand((unsigned)time(NULL));
    int x = rand() % 5 + 8;
    int y = rand() % 5 + 8;
    vector<vector<int>> score(y, vector<int>(x, 0));

    for (int i = 0; i < (y + 1) / 2; i++) {
        for (int j = 0; j < (x + 1) / 2; j++) {
            int Score = nrand(3);

            score[i][j] = Score;
            score[y - 1 - i][j] = Score;
            score[y - 1 - i][x - 1 - j] = Score;
            score[i][x - 1 - j] = Score;
        }
    }

    for (int i = 0; i < y; i++) {
        for (int j = 0; j < x; j++)
            cout << setw(4) << score[i][j];
        cout << endl;
    }

    return 0;
}
