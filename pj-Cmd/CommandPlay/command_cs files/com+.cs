﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

/*		<Memo List>
	マスの重み：1.0　0.7　0.4 
	重み同士が重なりあうと"(*0.4 *0.4 =) 0.016"の値もありうる

	関数（メソッド）の終わりに // function()_end
	
*/
namespace CommandPlay
{
	class Program
	{
		public static int[,] val;   // 得点
		public static int wid, hei; // マスの大きさ
		public static int myPx1, myPy1, myPx2, myPy2;   // 味方エージェント
		public static int enPx1, enPy1, enPx2, enPy2;   // てきエージェント

		public static double[,] Clone;  // 重み付け用
		public static int[,] Player;    // マスの取得状況
		public static bool[,] Trout;    // マス囲み

		static void Main(string[] args)
		{
			OpenFile();         // ファイル読み込み
			Initialization();   // 初期化

			input:
			int inTurn = 0;
			Console.Write("What Turns? -> ");

			try { inTurn = int.Parse(Console.ReadLine()); }     // ターン数入力
			catch (Exception e) { Console.WriteLine($"{e.Message}\n"); goto input; }    // int じゃなければ再入力

			int turn = 1;
			while (turn <= inTurn)
			{
				// val -> clone （初期化
				Array.Copy(val, Clone, val.Length);
				Weight(myPy1, myPx1); Weight(myPy2, myPx2); //Weight Attach

				SearchRoute.Search();     // ルート探索
				Print();
				Console.WriteLine($"Score");
				Console.WriteLine($"  ->  Ally : {Cal(1)} / Enemy : {Cal(-1)}");

				Console.Write($"\n{turn} Trun / {inTurn} Truns.");

				int h = 0;
				while (h++ < 4)
				{
					loop:
					Console.Write($"\n\nNo.{h}'s Player Location. ->  ");
					var ch = Console.ReadLine().ToArray();  // 移動先のマスの位置
					int flag = 0;  // 入力形式の判定

					int rlx, rly;
					try
					{
						rlx = (char.IsUpper(ch[0]) ? ch[0] + 32 - 'a' : ch[0] - 'a');   // 大文字なら小文字にして-'a'
						rly = ch[1] - '0';
					}
					catch { { Console.Write("Error. Please Retype."); goto loop; } }

					if (!(rlx >= 0 && rlx <= hei - 1)) flag = -1;
					if (ch.Length > 2 && (rly *= 10 + ch[2] - '0') > wid - 1) flag = -1;
					if (flag == -1) { Console.Write("Error. Please Retype."); goto loop; }

					// 移動可能でない場所を入力 -> 再入力
					if (CanMove(rlx, rly, (h - 1) * 2, myPx1, myPy1, myPx2, myPy2, enPx1, enPy1, enPx2, enPy2) == -1) goto loop;

					// 入力間違いがないかどうか
					Console.Write("Really? if it isn't, press N. -> ");
					if (Console.ReadKey().Key == ConsoleKey.N) goto loop;   // Nキーが押されたら再入力
					else // Nキー以外なら移動
					{
						switch (h)
						{
							case 1: LtoP(ref myPx1, ref myPy1, rlx, rly, 0); break;
							case 2: LtoP(ref myPx2, ref myPy2, rlx, rly, 0); break;
							case 3: LtoP(ref enPx1, ref enPy1, rlx, rly, 1); break;
							case 4: LtoP(ref enPx2, ref enPy2, rlx, rly, 1); Console.WriteLine(); break;
						}
					}
				}
				turn++;

				// a1 g8 g2 b10

				/* << Memo >> */
				{
					/*	
					 *	○ len ,
					 *	移動先のマスが敵のマスだったら		→ 現在地にとどまる && 移動先のマスを空にする
					 *	移動先のマスが味方・空のマスだったら→ 進む && 味方のマスにする
					 *					||	移動先のマス、myPx1などの現在地とを分ける必要あり？

					 *	 // ch[1]が2桁以上のとき？	-> 配列の長さが3

					 *	？ Confirm
					 *	敵のマスに行った場合、空のマスにできるか && 移動前と現在のマスを同じ位置に保てているかどうか

					 *	○ Think -> Success　-> another ○
					 *	味方と敵、それぞれの関数を作って、
					 *		case 1: myLP(myLx1, myLy1); case 2: myLP(myLx2, myLy2);
					 *		case 3: enLP(enLx1, enLy1); case 4: enLP(enLx2, enLy2);		簡略化

					 *　○ Success
					 *　入力値に、文字と数字が含まれているか
					 *	（文字：'a' <> 'a'+hei || 'A' <> 'A'+hei ^ 数字：1 <>1+wid）

					 *	○Think
					 *	# 一マス先もしくは、その場にとどまる場合
					 *		→ そのまま続ける
					 *	# 移動不可能なマスへの移動	// 現在地から2マス以上マス先なら再入力
					 *		→ goto loop;（再入力させる）
					 *	各エージェントの現在地に対して、移動先が2マス先などを参照しているかどうかを調べる
					 *		移動不可の場合はやり直しさせる

					 *	○ Success
					 *	ルート候補を'-1','1'ではなく、'A1','G10'などと表示する
					 *		→ 表示例）  
					 *					A1  B2  C1 -> Score : ○

					 *	なんかルート候補に＠が表示されるというバグみたことが…
					 *		@ マス外(!(A <> A+hei-1 / 0 <> 0+wid-1))を参照したときは移動先を再入力させる
					 *			→ 移動先のマス判定はちゃんと動作してるはず
					 *			→ MaxRoute()でマス外を参照するとき候補にしない　// ← ができてない
					 *			

					 *	重み付与は、現在地が動くたびにつける
					*/
				}
				{
					/*
					 *	配列外を参照しているものがなぜルート候補としてあるのか？
					 *	それらをルート候補としないためにはどうすればよいか？
					 *	　また、ルート表示のさい、それらを除いたものだけを表示するにはどうすればよいか？
					 *	　（3て先まですべて正しいルートはきちんと移動できる ← 確認済み）
					 *		→ 移動できないもの以外は正しいルートなのではないか？
					 *		→ 正しいルートであるならば、ok？

					 *	移動出来ないものを除外して、正しいルート＆得点が上位3つものもの個数をかえす(変数：r_n)
					 *	ルート表示の際は移動出来ないものかどうか判断し、添字をずらしてr_nまでくりかえす
					 *	(移動出来ないかどうかは、while（移動出来ないものがある間）{ 添字++ }くりかえす)

					 *	List.Resize()はできないのか？
					 *	  → できるなら、Player,Troutを初期化するのではなく、領域だけ確保する
					*/
				}

				/*
				 *	MaxRoute()は全ルートを "ルート(route)"と"点数(routeNum)"を格納する
				 *	ShowRoute() or 別関数で3て先まで移動できる場所を指定している場合のみ、表示
				 *		→ && routeNumが異なる場合のみcount++、count < 3まで繰り返す
				 */
			}
		}

		/// <summary>
		/// 移動可能マスかどうかの判定
		/// </summary>
		/// <param name="rlx"></param>
		/// <param name="rly"></param>
		/// <param name="t"></param>
		/// <param name="P"></param>
		/// <returns></returns>
		static int CanMove(int rlx, int rly, int t, params int[] P)
		{
			if (!(rlx - P[t] >= -1 && rlx - P[t] <= 1 && rly - P[t + 1] >= -1 && rly - P[t + 1] <= 1))
			{ Console.Write("Error. Cannot move there."); return -1; }
			return 0;
		}// CanMove()_end

		///	<Verification>
		/// 味方マスが敵マスを指定した場合、マスを打ち消すかどうか
		///		マスを上書きするかどうか

		/// <summary>
		/// 移動指定マス -> 現在地マス
		/// </summary>
		/// <param name="Px"></param>
		/// <param name="Py"></param>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="check"></param>
		static void LtoP(ref int Px, ref int Py, int x, int y, int check)
		{
			switch (check)
			{
				case 0: // 味方のとき
					if (Player[x, y] == Box.E) Player[x, y] = Box.V;    // 移動先が敵のマス -> Empty
					else (Px, Py, Player[x, y]) = (x, y, Box.M);    // 敵以外のマス -> Mine
					break;
				case 1: // 敵のとき
					if (Player[x, y] == Box.M) Player[x, y] = Box.V;    // 移動先が味方マス -> Empty
					else (Px, Py, Player[x, y]) = (x, y, Box.E);    // 味方以外のマス -> Enemy
					break;
			}
		}// LtoP()_end

		/// <summary>
		/// ファイルデータの読み取り
		/// </summary>
		static void OpenFile()
		{
			try
			{
				string OutPath = null;
				//OutPath = @"C:\Users\Student\Downloads\";                   // School
				OutPath = @"C:\Users\岡村一矢\Downloads\Chrome_Download\";  // Mine

				//string rnPath = @"C:\Users\岡村一矢\Dropbox\program\StudyC#\Procon\rndata.csv";

				using (var sr = new StreamReader(OutPath + "data.csv"))
				{
					string[] cut = sr.ReadToEnd().Split(' ', ':');  // ' ', ':'で区切りを入れる
					var read = new List<string>();

					foreach (var row in cut) read.Add(row);
					read.RemoveAll(x => x == "");   // nullを削除

					var temp = read.ConvertAll(x => int.Parse(x));  // string -> int
					int[] arr = temp.ToArray();     // List<int> -> int[]
					hei = arr[0]; wid = arr[1];     // width, height
					val = new int[hei, wid];

					int it = 2; // arr 走査用
					for (int i = 0; i < hei; i++)   // arr から val に
						for (int j = 0; j < wid; j++, it++)
							val[i, j] = arr[it];

					//エージェントの位置データを取得
					int pos1, pos2, arlen = arr.Length;

					if ((arr[arlen - 2] -= 1) < (arr[arlen - 4] -= 1)) { myPx1 = arr[arlen - 2]; myPx2 = arr[arlen - 4]; pos1 = arr[arlen - 2]; }
					else { myPx1 = arr[arlen - 4]; myPx2 = arr[arlen - 2]; pos1 = arr[arlen - 4]; }

					if ((arr[arlen - 1] -= 1) < (arr[arlen - 3] -= 1)) { myPy1 = arr[arlen - 1]; myPy2 = arr[arlen - 3]; pos2 = arr[arlen - 1]; }
					else { myPy1 = arr[arlen - 3]; myPy2 = arr[arlen - 1]; pos2 = arr[arlen - 3]; }

					// 敵の初期位置
					enPx1 = hei - 1 - pos1;
					enPy1 = pos2;
					enPx2 = pos1;
					enPy2 = wid - pos2 - 1;

					OutputIntoFile();   // 各データの書き込み
				}
			}
			catch (Exception e) { Console.WriteLine(e.Message); Environment.Exit(0); }
		}// OpenFile()_end

		/// <summary>
		/// マスの初期化
		/// </summary>
		static void Initialization()
		{
			Clone = new double[hei, wid];           // Cloneの領域確保
			Array.Copy(val, Clone, val.Length);     // valの値コピー				

			Player = new int[hei, wid]; // Partitioning
			Trout = new bool[hei, wid];

			Player = new int[hei, wid];
			for (int i = 0; i < hei; i++)   // Empty Trout
				for (int j = 0; j < wid; j++)
					Player[i, j] = Box.V;

			// 開始時のマス
			Player[myPx1, myPy1] = Box.M; Player[myPx2, myPy2] = Box.M;
			Player[enPx1, enPy1] = Box.E; Player[enPx2, enPy2] = Box.E;

			ShowTrout(val, nameof(val));
		}// Initialization()_end

		/// <summary>
		/// マスの取得状況
		/// </summary>
		/// <param name="i"></param>
		/// <param name="j"></param>
		/// <param name="Pnum"></param>
		static void Score(int i, int j, int Pnum)
		{
			int[] vx = { 1, 0, -1, 0 }, vy = { 0, 1, 0, -1 };

			if (i == 0 && j == 0)
			{
				for (int x = 0; x < hei; x++)
					for (int y = 0; y < wid; y++)
						Trout[x, y] = true;
			}
			if (Player[i, j] != Pnum) Trout[i, j] = false;
			else return;

			for (int x = 0; x < 4; x++)
			{
				if (0 <= i + vx[x] && i + vx[x] < hei && 0 <= j + vy[x] && j + vy[x] < wid)
					if (Player[i + vx[x], j + vy[x]] != Pnum && Trout[i + vx[x], j + vy[x]])
						Score(i + vx[x], j + vy[x], Pnum);
			}
		}// Score()_end

		/// <summary>
		/// 各チームの得点を計算する
		/// </summary>
		/// <param name="Pnum"></param>
		/// <returns></returns>
		static int Cal(int Pnum)
		{
			int ret = 0;
			for (int i = 0; i < hei; i++)
			{
				for (int j = 0; j < wid; j++)
					if (i == 0 || i == hei - 1 || j == 0 || j == wid - 1)
						Score(i, j, Pnum);
			}

			for (int i = 0; i < hei; i++)
			{
				for (int j = 0; j < wid; j++)
				{
					if (Trout[i, j] && Player[i, j] == Pnum)
						ret += val[i, j];
					else if (Trout[i, j])
						ret += (val[i, j] > 0 ? val[i, j] : val[i, j] * -1);
				}
			}
			return ret;
		}// Cal()_end

		/// <summary>
		/// 二次元配列を列挙する
		/// </summary>
		static void Print()
		{
			for (int i = 0; i < hei; i++)
			{
				for (int j = 0; j < wid; j++)
					Console.Write(Player[i, j] == 1 ? "●" : Player[i, j] == -1 ? "○" : "　");
				Console.WriteLine();
			}
		}// Print()_end

		/// <summary>
		/// 二次元配列を列挙する
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="array"></param>
		/// <param name="name"></param>
		static void ShowTrout<T>(T[,] array, string name)
		{
			char CharN = 'A';
			Console.WriteLine($"Output {name}\n");      // 変数名 （見出し）

			Console.Write("     ");
			for (int i = 0; i < wid; i++) Console.Write($"{i,5}");
			Console.Write("\n     ");
			for (int i = 0; i < wid; i++) Console.Write("-----");
			Console.WriteLine();

			for (int i = 0; i < hei; i++)
			{
				Console.Write($" {CharN++} | ");
				for (int j = 0; j < wid; j++) Console.Write($"{array[i, j],5}");
				Console.WriteLine();
			}
			Console.WriteLine();
		}// ShowTrout()_end

		/// <summary>
		/// Playerから3マス先までの重みを付ける
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		static void Weight(int x, int y)
		{
			double[] om = { 1.0, 0.7, 0.4 };    // 重み（倍率
			int ls = 1;     // ループ用 ＆ 自分を中心とした〇マス先

			while (ls <= 3)
			{
				for (int i = -ls; i < ls; i++)
				{
					/* ← ↑ → ↓ の順 */
					// （それぞれ） 配列外を参照していないなら重みを付与する

					if (!(y + i < 0 || y + i > hei - 1 || x - ls < 0))      // ← Not Error.なら
						Clone[y + i, x - ls] = val[y + i, x - ls] * om[ls - 1];

					if (!(y - ls < 0 || y - ls > hei - 1 || x + i + 1 < 0 || x + i + 1 > wid - 1))  // ↑ Not Error.なら
						Clone[y - ls, x + i + 1] = val[y - ls, x + i + 1] * om[ls - 1]; // i + 1

					if (!(y + i + 1 < 0 || y + i + 1 > wid - 1 || y + i + 1 > hei - 1 || x + ls > wid - 1)) // → Not Error.なら
						Clone[y + i + 1, x + ls] = val[y + i + 1, x + ls] * om[ls - 1]; // i + 1

					if (!(y + ls > hei - 1 || x + i < 0 || x + i > wid - 1))    // ↓ Not Error.なら
						Clone[y + ls, x + i] = val[y + ls, x + i] * om[ls - 1];
				}
				ls++;
			}
		}// Weight()_end

		/// <summary>
		/// 各データをCSVファイルとして出力する
		/// </summary>
		static void OutputIntoFile()
		{
			string OutPath = null;
			//OutPath = @"C:\Users\Student\Dropbox\program\StudyC#\Procon\TFP_WriteFile\";    // School
			OutPath = @"C:\Users\岡村一矢\Dropbox\program\StudyC#\Procon\TFP_WriteFile\";   // Mine
			try
			{
				var Square = new StreamWriter(OutPath + "Square.csv", false);
				var Score = new StreamWriter(OutPath + "Score.csv", false);
				var Agent = new StreamWriter(OutPath + "Agent.csv", false);

				Square.Write(hei + "," + wid);      // マスの縦横 書き出し

				foreach (var i in val) Score.Write(i + ",");

				// エージェントの位置 書き出し
				int[,] pos = { { myPx1, myPy1 }, { myPx2, myPy2 }, { enPx1, enPy1 }, { enPx2, enPy2 } };
				foreach (var i in pos) Agent.Write(i + ",");

				Square.Close(); Score.Close(); Agent.Close();   // ファイルを閉じる
			}
			catch (Exception e) { Console.WriteLine(e.Message); Environment.Exit(0); }
		}// OutputIntoFile()_end



		/// <summary>
		/// マスの定義 My : 1 / V(None) : 0 / Enemy : -1
		/// </summary>
		struct Box
		{
			public static int M = 1,    // 味方のマス
							  V = 0,    // 誰のマスでもない
							  E = -1;    // 敵のマス
		}// struct.Box_end

		/// <summary>
		/// class Algorithm
		/// </summary>
		class SearchRoute
		{
			// route / public static から local で 関数わたしで解決するかも…（前回のを引き継いでるから…？
			public static double[,,] route = new double[729, 3, 2]; // ルートを記憶する配列
			public static double[,,] route_Ini = new double[729, 3, 2];
			public static double[] routeNum = new double[729];      // 得点を記憶する配列
			public static double[] routeNum_Ini = new double[729];      // 得点を記憶する配列

			//public static bool[] tf = new bool[routeNum.Length];	// <bool check>

			/// <summary>
			/// 3て先までのルートを求める
			/// </summary>
			public static void Search()
			{
				Array.Copy(route_Ini, route, route.Length);
				Array.Copy(routeNum_Ini, routeNum, routeNum.Length);
				// 1人目
				MaxRoute(Clone, myPx1, myPy1);
				ShowRoute("1", myPx1, myPy1);

				Console.WriteLine("/ ==================================== /\n");   // ただの区切り（みやすくするため

				// 2人目
				MaxRoute(Clone, myPx2, myPy2);
				ShowRoute("2", myPx2, myPy2);

				ShowTrout(Clone, "Clone");
				ShowTrout(Player, "Player");
			}// Search()_end

			static int nl = 1;
			static int toji = 0;
			/// <summary>
			/// ルートを表示する
			/// </summary>
			/// <param name="name"></param>
			/// <param name="Px"></param>
			/// <param name="Py"></param>
			static void ShowRoute(string name, int Px, int Py)
			{
				/** ここマジ意味わからんわ **/
				int n;
				//if (nl == 1) { n = Several(myPx1, myPy1); nl++; }	// <bool check>?
				//else n = Several(myPx2, myPy2);

				n = Several(Px, Py);
				/** ====================== **/
				// もうアルゴリズム書き換えた方が早くね？

				/// 検証
				/// あらかじめBox.Mを2つ以上設定して動作するかどうか
				int nx, ny;
				Console.WriteLine($"Output No.{name} Player\n");
				for (int i = 0; i < n; i++)     // ルートの表示
				{
					(nx, ny) = (Px, Py);
					for (int j = 0; j < route.GetLength(1); j++)
					{
						nx += (int)route[i, j, 0];
						ny += (int)route[i, j, 1];
						//if (tf[i])	// <bool check>
						Console.Write($" {(char)(nx + 'A')}{ny.ToString(),-2} ");
						//Console.Write($" {nx,2} {ny.ToString(),2} ");
					}
					//if (tf[i])	// <bool check>
					Console.WriteLine($" ->  Score : {routeNum[i]}\n");
				}
			}// ShowRoute()_end


			/// <summary>
			/// ルート候補数を調べる
			/// </summary>
			/// <param name="Px"></param>
			/// <param name="Py"></param>
			/// <returns></returns>
			static int Several(int Px, int Py)
			{

				bool[,] judge = new bool[routeNum.Length, 3];   // routeが (true / false) かどうか
				int nx, ny;
				for (int k = 0; k < routeNum.Length; k++)
				{
					(nx, ny) = (Px, Py);
					for (int i = 0; i < 3; i++)
					{
						nx += (int)route[k, i, 0];
						ny += (int)route[k, i, 1];
						//judge[k, i] = (!(nx < 0 || nx > hei - 1 || ny < 0 || ny > wid - 1)) ? true : false;	// <bool check>
						//Console.WriteLine($"nx : {nx}/ny : {ny}/judge : {judge[k, i]} -> tf : {tf[k]}");
					}
					//tf[k] = (judge[k, 0] && judge[k, 1] && judge[k, 2]) ? true : false;	// <bool check>
				}


				// 昇順ソート
				int cnt = 0;// 得点が３番目に大きいルートまでループするための変数
				int r_n;// 候補数を数える変数
				for (r_n = 0; cnt < 3; r_n++)
				{
					for (int j = routeNum.Length - 1; j > r_n; j--)
					{
						if (routeNum[j] > routeNum[j - 1])
						{
							// 得点入れ替え
							(routeNum[j - 1], routeNum[j]) = (routeNum[j], routeNum[j - 1]);

							// ルート入れ替え
							for (int k = 0; k < route.GetLength(1); k++)
								for (int l = 0; l < route.GetLength(2); l++)
									(route[j, k, l], route[j - 1, k, l]) = (route[j - 1, k, l], route[j, k, l]);
						}
					}

					// 得点の候補数を数える
					cnt = 1;
					var n = routeNum[0];
					for (int j = 1; j <= r_n; j++)
					{
						//while (!tf[j]) j++;	// <bool check>

						if (n != routeNum[j])
						{
							cnt++;
							n = routeNum[j];
							Console.Write($"{toji++} ");    // 入ったかどうか
						}
					}
				}
				Console.WriteLine();    //toji消したらけす
				return r_n;
				// r_nなしで、表示するときにcount < 3（○ < routeNum.Lnegth）のあいだ繰り返す
				// < 0, > hei-1, > wid-1 をListでRemoveAll()？
			}

			/// <summary>
			/// ルートの候補を見つける
			/// </summary>
			/// <param name="Field"></param>
			/// <param name="myPointX"></param>
			/// <param name="myPointY"></param>
			/// <returns></returns>
			public static void MaxRoute(double[,] Field, int myPointX, int myPointY)
			{
				// i, k, m -> Vertical
				// j, l, n -> Horizontal

				int RouteCnt = 0;   //ルート数を数える変数
				int oneM = 2, twoM = 2, thiM = 2,    // マスの定義と被らないように -1 
					x, y;
				// Search
				for (int i = -1; i <= 1; i++)
				{
					for (int j = -1; j <= 1; j++)
					{
						x = myPointX + i;   // Easy To See Code
						y = myPointY + j;

						// Out Of Range なら continue
						if (x < 0 || x > hei - 1 || y < 0 || y > wid - 1) continue;
						// $+1 is (M, E, V)
						oneM = Player[x, y];

						// $+1 -> M || E なら continue
						if (oneM == Box.M/* || oneM == Box.E*/) continue;

						// $+1 -> V なら
						else if (oneM == Box.V)
						{
							Player[x, y] = Box.M;       // $+1 を M に
							for (int k = -1; k <= 1; k++)
							{
								for (int l = -1; l <= 1; l++)
								{
									// Out Of Range なら continue
									if (x + k < 0 || x + k > hei - 1 || y + l < 0 || y + l > wid - 1) continue;

									// $+2 is (M, E, V)
									twoM = Player[x + k, y + l];

									// $+2 -> M なら continue
									if (twoM == Box.M) continue;

									// $+2 -> V なら
									else if (twoM == Box.V)
									{
										// マスを M に
										Player[x + k, y + l] = Box.M;

										for (int m = -1; m <= 1; m++)
										{
											for (int n = -1; n <= 1; n++)
											{
												// Out Of Range なら continue
												if (x + k + m < 0 || x + k + m > hei - 1 || y + l + n < 0 || y + l + n > wid - 1) continue;
												// $+3 is (M, E, V)
												thiM = Player[x + k + m, y + l + n];

												// $+3 -> M なら continue
												if (thiM == Box.M) continue;

												// $+3 -> V ならルートを記憶
												else if (thiM == Box.V)
												{
													// Storing Route
													RouteIn(i, j, k, l, m, n, RouteCnt);

													// Total vlaue
													routeNum[RouteCnt] = Field[x, y] + Field[x + k, y + l] + Field[x + k + m, y + l + n];
													//RouteCnt++;
													RouteCnt++;
												}

												// $+3 -> E ならルートを記憶
												else if (Player[x + k + m, y + l + n] == Box.E)
												{
													// Storing Route
													RouteIn(i, j, k, l, 0, 0, RouteCnt);

													// Total value
													routeNum[RouteCnt] = Field[x, y] + Field[x + k, y + l];
													//RouteCnt++;
													RouteCnt++;
												}
											}
										}
									}
									// $+2 -> E ならルートを記憶
									else if (twoM == Box.E)
									{
										// Storing Route
										RouteIn(i, j, 0, 0, k, l, RouteCnt);

										// Total value
										routeNum[RouteCnt] = Field[x, y] + Field[x + k, y + l];
										//RouteCnt++;
										RouteCnt++;
									}
									// $+2 = towM
									Player[x + k, y + l] = twoM;
								}
							}
						}
						// $+1 -> E なら
						else if (oneM == Box.E)
						{
							// $+1 を M に
							Player[x, y] = Box.M;
							for (int k = -1; k <= 1; k++)
							{
								for (int l = -1; l <= 1; l++)
								{
									// $+2 -> M なら continue
									if (twoM == Box.M) continue;

									// $+2 -> V ならルートを記憶
									else if (twoM == Box.V)
									{
										// Storing Route
										RouteIn(0, 0, i, j, k, l, RouteCnt);

										// Total value
										routeNum[RouteCnt] = Field[x, y] + Field[x + k, y + l];
										//RouteCnt++;
										RouteCnt++;
									}
									// $+2 -> E ならルートを記憶
									else if (twoM == Box.E)
									{
										// Storing Route
										RouteIn(0, 0, i, j, 0, 0, RouteCnt);

										// Total value
										routeNum[RouteCnt] = Field[x, y];
										//RouteCnt++;
										RouteCnt++;
									}
								}
							}
						}
						// $+1 = oneM
						Player[x, y] = oneM;
					}
				}
			}

			// ルート代入
			private static void RouteIn(int x1, int y1, int x2, int y2, int x3, int y3, int cnt)
			{
				route[cnt, 0, 0] = x1;
				route[cnt, 0, 1] = y1;
				route[cnt, 1, 0] = x2;
				route[cnt, 1, 1] = y2;
				route[cnt, 2, 0] = x3;
				route[cnt, 2, 1] = y3;
			}// RouteCnt+=RouteIn()_end

		}// class.SearchRoute_end

	}// class.Program_end
}
// とりあえず完成版 17/23:59