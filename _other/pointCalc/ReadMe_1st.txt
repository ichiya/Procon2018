得点計算プログラムの使い方。このプログラムは本選の選手ではなく，審判員が利用するものです。
How to use "Point Calculate Program". Please note this program is not be for contestants 
but for judges. 

1 index.htmlをダブルクリックする。またはブラウザ（Firefox, Chrome, IEなど）で
index.htmlを開いてください。
1. Double click the "index.html" file, or open the "index.html" with a Browser: 
   Firefox, Chrome, IE and so on. 
   
2. api-keyの欄に入力してください。 api-keyは指導教員宛に9月6日に電子メールでお知らせします。
2. Fill out api-key, which will be informed to supervisors via e-mail on Sep. 6.  

3. チーム名を入力してください
3. Fill out team names. 

4. 両チームのQRInfoを入力してください。例として，以下の二つを示します。
   QRinfo for team1 8 11:-2 1 0 1 2 0 2 1 0 1 -2:1 3 2 -2 0 1 0 -2 2 3 1:1 3 2 1 0 -2 0 1 2 3 1:2 1 1 2 2 3 2 2 1 1 2:2 1 1 2 2 3 2 2 1 1 2:1 3 2 1 0 -2 0 1 2 3 1:1 3 2 -2 0 1 0 -2 2 3 1:-2 1 0 1 2 0 2 1 0 1 -2:2 2:7 10:
   QRinfo for team2 8 11:-2 1 0 1 2 0 2 1 0 1 -2:1 3 2 -2 0 1 0 -2 2 3 1:1 3 2 1 0 -2 0 1 2 3 1:2 1 1 2 2 3 2 2 1 1 2:2 1 1 2 2 3 2 2 1 1 2:1 3 2 1 0 -2 0 1 2 3 1:1 3 2 -2 0 1 0 -2 2 3 1:-2 1 0 1 2 0 2 1 0 1 -2:2 10:7 2:
4. Fill out QRInfo of both teams: for example, 
   QRinfo for team1 8 11:-2 1 0 1 2 0 2 1 0 1 -2:1 3 2 -2 0 1 0 -2 2 3 1:1 3 2 1 0 -2 0 1 2 3 1:2 1 1 2 2 3 2 2 1 1 2:2 1 1 2 2 3 2 2 1 1 2:1 3 2 1 0 -2 0 1 2 3 1:1 3 2 -2 0 1 0 -2 2 3 1:-2 1 0 1 2 0 2 1 0 1 -2:2 2:7 10:
   QRinfo for team2 8 11:-2 1 0 1 2 0 2 1 0 1 -2:1 3 2 -2 0 1 0 -2 2 3 1:1 3 2 1 0 -2 0 1 2 3 1:2 1 1 2 2 3 2 2 1 1 2:2 1 1 2 2 3 2 2 1 1 2:1 3 2 1 0 -2 0 1 2 3 1:1 3 2 -2 0 1 0 -2 2 3 1:-2 1 0 1 2 0 2 1 0 1 -2:2 10:7 2:

5. "create field"ボタンをクリックしてください。フィールドが生成されます。
5. Click the "create field" button. The field appears. 

6. タイルカラー（青，赤，白）を選んでください。白は除去を意味します。
6. Select a tile color: red, blue, or white. 
   The white measn erase of tiles. 

7. マウスの左クリックでマス達を塗ってください。
7. Paint grids with left click of your mouse. 

8. マス達を塗り終わったら "calc current points" ボタンをクリックしてください。
   各チームのタイルポイント，領域ポイント，合計ポイントが表示されます。
   領域ポイントに相当するマス達が点滅します。
8. After you fill out grids, click "calc current points" button. 
   You can see the tile, territory, and total points of both teams' fields. 
   Please note the territory grids of each field flush. 
 
9. バグを発見した場合は jimu29@procon.gr.jp までご連絡ください。
9. When you find bugs, please let us know at jimu29@procon.gr.jp. 
   