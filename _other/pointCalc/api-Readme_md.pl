得点計算APIの使い方
How to use "Point Calculate API"

1. APIのエンドポイントは https://42isf6z498.execute-api.ap-northeast-1.amazonaws.com/dev です．
1. The endpoint of this API is https://42isf6z498.execute-api.ap-northeast-1.amazonaws.com/dev

2. このエンドポイントに対して，以下の形式に則ったJSONをbodyに含め，POSTしてください．
この際，ヘッダには "content-type":"application/json"及び，"x-api-key":************ を必ず含めてください．
************は指導教員にE-mailで通知されます。
また、tilesで指定するy,xは0-indexです。
2. POST JSON body as below.
When post this data, you have to include header "content-type": "application/json", and "x-api-key":********.
************ will be informed to supervisors via e-mmail. 
"y" and "x" in "tiles" are 0-indexed.
{
    "field":{ 
        "scores" : [ Integer ],
        "height": Integer,
        "width": Integer
    },
    "teams":[
        { "tiles" : [
            {"y": Integer,"x": Integer}
        ]},
        { "tiles" : [
            {"y": Integer,"x": Integer}
        ]}
    ]
}

3. レスポンスは以下の形式に則ったjsonが返却されます．"data"がnullの場合はPOST内容に問題があった場合です．"error"をみて適切なjsonを再度POSTしてください．
3. The api server will return json response as below. 
{
    "data":{
        [{"tile_point": Integer,
         "territory_point": Integer,
          "tile_area": [ Integer ]
        }]
    },
    "response_id": String,
    "error": String
}

4. 例として，上の得点計算で入力したQRInfoを再現すると以下のようなcurlコマンドで実現できます．
4. For the sample, you can run a curl command like this. 
curl -H 'content-type:application/json' -H 'x-api-key:************' \
-X POST -d '{"field":{"scores":[-2, 1, 0, 1, 2, 0, 2, 1, 0, 1, -2, 
1, 3, 2, -2, 0, 1, 0, -2, 2, 3, 1, 
1, 3, 2, 1, 0, -2, 0, 1, 2, 3, 1, 
2, 1, 1, 2, 2, 3, 2, 2, 1, 1, 2, 
2, 1, 1, 2, 2, 3, 2, 2, 1, 1, 2, 
1, 3, 2, 1, 0, -2, 0, 1, 2, 3, 1, 
1, 3, 2, -2, 0, 1, 0, -2, 2, 3, 1, 
-2, 1, 0, 1, 2, 0, 2, 1, 0, 1, -2], "height":8, "width":11}, 
"teams":[{"tiles":[{"y": 1, "x": 1}, {"y": 6, "x": 9}]}, 
{"tiles":[{"y": 1, "x": 9}, {"y": 6, "x": 1}]}]}' https://42isf6z498.execute-api.ap-northeast-1.amazonaws.com/dev