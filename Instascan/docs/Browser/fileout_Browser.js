'use strict';

function readFile() {
	var result = ((document.getElementsByClassName("scans"))[0].getElementsByTagName("li"))[0].textContent;

	let downloadFile = (name, content) => {
		var blob = new Blob([content], {
			type: 'text/plain'
		});
		var blobURL = window.URL.createObjectURL(blob);

		var a = document.createElement('a');
		a.href = blobURL;
		a.download = name;
		a.click();
	}
	var fileName = "data.csv";
	downloadFile(fileName, result);
}
