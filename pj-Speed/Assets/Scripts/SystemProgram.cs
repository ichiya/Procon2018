﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System;
using UnityEngine.UI;
using static UnityEngine.Debug;

/**
	* QRコードは, hei: 11 , wid:8 を"-90"度回転させる
		→ 横に向けた配列を用意して, 再代入
	* 順番関係なく色を付けていく
		→ 味方のみを交互に
	* 相手の行動は入力しない(一応用意はするかも？)
		→ ボタンで切り替えればできなくもない
			ただし、味方の移動が青色になるだけ
	* ターン数はカウントしない（できない）
		→ 更新なんてしない
	* 敵位置は一切考えない（初期位置も） // 変数には記憶
	* 自エージェントを交互に操作する
		→ Round, PlayerCount -> %2した値を round, pc4 でgetする
	* 
	*/

public class SystemProgram : MonoBehaviour
{
	// GUI
	public Text allyText;           // 味方の点数
	public Text enemyText;          // 敵の点数
	public Text turnText;           // ターン数
	public Text directionText;      // 1人目の方向
	public Text directionText2;     // 2人目の方向
	public Text message;            // メッセージ
	public Text searchText;         // 1人目のルート
	public Text searchText2;        // 2人目のルート
	public Text currentPlayerText;  // 操作中のプレイヤー
	public Text warningText;        // 警告文

	private static Text mess;       // MessageText コンポーネント
	public static Canvas dialog;

	public static UIs it1, it2, txts, raws, im;  // GUI コンポーネント
	private static List<string> eachRoute = new List<string>();       // 行動パターン
	private static List<string[]> overlap = new List<string[]>();   // 1手目のルート候補

	public static Dictionary<string, int> Buttons = new Dictionary<string, int>();      // ボタンの情報
	public static Dictionary<string, string> toJp = new Dictionary<string, string>();   // 移動方向

	private static string estr;   // エラーメッセージ
	private static string backUpPath;   // バックアップ先のパス
										//private static StreamWriter _backSR;

	private static bool blinked;    // 点滅させるかどうか
	private static bool everyTurn = true;  // 1ターン経過したかどうか
	private bool visibled = false;   // オブジェクトの状態

	public static int PlayerCount { get; set; } = 0;    // 操作するプレイヤーの番号
	public static int Round = 0;
	private static int round => Round % 2;

	public static int candidateNum,     // 1人目のルート候補数
					  turnCount = 1;    // ターン数
										//public static int Pc4 => PlayerCount % 4; // プレイヤー番号を返す
	public static int Pc4 => PlayerCount % 2; // プレイヤー番号を返す
	public static int[,] value; // スコア
	public static int wid, hei; // サイズ

	public static int myPx1, myPy1, myPx2, myPy2,   // 味方
					  enPx1, enPy1, enPx2, enPy2;   // 敵

	public static double[,] firstWeight,    // 重み付け - 1P
							secondWeight;   //			 - 2P
	public static int[,] player;    // マスの取得状況
	public static bool[,] board;    // マス囲み判定


	public static int[,] cpy;

	void Start()
	{
		mess = GameObject.Find("MessageText").GetComponent<Text>(); // component
		mess.gameObject.SetActive(false);   // not active 
		backUpPath = $"{Application.dataPath}/backup.txt";

		// 条件分岐（バックアップ / 新規）
		if (StartUp.backed)
		{
			OpenBackUp();
			// エラーが発生したなら
			if (mess.isActiveAndEnabled) { message.text = estr; return; }

			Initialization();       // データの初期化(共通部分のみ)
			NewFunc.CreateButton(); // 盤面生成

			// 色のついていた部分に色付け
			var nm = "";
			for (int i = 0; i < hei; i++)
				for (int j = 0; j < wid; j++)
				{
					nm = $"Button{i + 1:D2}{j + 1:D2}";
					GameObject.Find(nm).GetComponent<Button>().image.color
						= (Buttons[nm] == 1) ? ButtonClick.red :
						  (Buttons[nm] == 0) ? Color.white : ButtonClick.blue;
				}

			// 操作済み番号を黄色に
			for (int i = 0; i < 4; i++)
				if (OperatePlayer.Enabled[i])
					OperatePlayer.PlayerNumber[i].image.color = Color.yellow;
		}
		else
		{
			OpenFile(); // ファイルを開く
			if (mess.isActiveAndEnabled)    // エラーが発生したなら
			{
				// メッセージを表示して終了
				message.text = estr;
				return;
			}
			Initialization();       // データの初期化(共通部分のみ)

			// -- not 共通部分 --
			for (int i = 0; i < hei; i++)
				for (int j = 0; j < wid; j++)
					Buttons.Add($"Button{i + 1:D2}{j + 1:D2}", 0);

			Array.Clear(player, PlayerColor.N, player.Length);
			player[myPx1, myPy1] = PlayerColor.A; player[myPx2, myPy2] = PlayerColor.A;
			//player[enPx1, enPy1] = PlayerColor.E; player[enPx2, enPy2] = PlayerColor.E;

			NewFunc.CreateButton(); // 盤面生成
		}
		Apply();
	}// end of Start

	private void Update()
	{
		// ターンはじめ && 点滅させるなら
		if (blinked && everyTurn)
		{
			everyTurn = false;
			// リスタート
			StopCoroutine("Blink");
			StartCoroutine("Blink");
		}
	}// end of Update

	/// 変更を適用する
	/// @ memo
	///		得点、ターン数の更新
	///		ルート探索
	///		現在地の更新
	public static void Apply()
	{
		// 盤面のデータを更新する
		for (int i = 0; i < hei; i++)
			for (int j = 0; j < wid; j++)
				player[i, j] = Buttons[$"Button{i + 1:D2}{j + 1:D2}"];

		// テキストの更新
		txts.Ally.text = $"{Cal(1)}";
		//txts.Enemy.text = $"{Cal(-1)}";
		//txts.Turn.text = $"{turnCount} Turn";

		PaintToLocation();

		// 操作プレイヤー以外のリストにマスクする
		raws.raw.gameObject.SetActive(Pc4 != 0);
		raws.raw2.gameObject.SetActive(Pc4 != 1);

		// 次のプレイヤー情報
		ChangeColor.JudgeNextColor();
		ButtonClick.SetCurrent();

		// バックアップを保存
		using (var _backSR = new StreamWriter(backUpPath))
		{
			// マスの得点などの情報はdata.csvから？
			/// 現在地
			/// マスの取得状況（得点 -> Cal() ）
			/// 操作状況（operateplayer）
			/// pc4
			/// ターン

			// current
			_backSR.Write($"{myPx1},{myPy1},{myPx2},{myPy2},{enPx1},{enPy1},{enPx2},{enPy2},");

			// マスの取得状況
			// 1: A / 0: N / -1: E
			// 盤面のカラーをつける
			// openfileの中じゃなくて Startの初めで条件分岐
			// Buttonsにreadした結果をadd
			foreach (var i in Buttons)
				_backSR.Write($"{i.Value},");

			// pc4
			_backSR.Write($"{Pc4},");

			// turn
			_backSR.Write($"{turnCount}");

			// operate
			for (int i = 0; i < 4; i++)
				_backSR.Write($",{((OperatePlayer.Enabled[i]) ? 0 : 1)}");

		}

		// 1ターンごと(4巡したら)
		if (round == 0)
		{
			everyTurn = true;

			// Destroy Button
			var objs = GameObject.FindGameObjectsWithTag("RouteNumber");
			foreach (var obj in objs) Destroy(obj);

			// ルート探索
			SearchRoute.Search();
			blinked = false;
			// 指示方向
			ShowMoveInfo(it1, 0, true);
			ShowMoveInfo(it2, candidateNum, false);

			// 二人の距離が3以下になったら
			if (Mathf.Abs(myPx1 - myPx2) <= 3 && Mathf.Abs(myPy1 - myPy2) <= 3)
				txts.Warn.gameObject.SetActive(true);   // 注意文の表示
		}
	}// end of Apply

	/// プレイヤー位置に色をつける
	private static void PaintToLocation()
	{
		var btns = GameObject.FindGameObjectsWithTag("ScoreBoard");
		foreach (var obj in btns)
		{
			// 現在地を示す色をなくす
			var btn = obj.GetComponent<Button>();
			if (btn.image.color == ButtonClick.cyan) btn.image.color = ButtonClick.blue;
			if (btn.image.color == ButtonClick.magenta) btn.image.color = ButtonClick.red;
			// テキストも戻す
			btn.GetComponentInChildren<Text>().color = Color.black;
		}

		var P = ButtonClick.GetAllLocation();
		for (int i = 0; i < 4; i++)
		{
			// 現在地に色を付ける
			var obj = GameObject.Find($"Button{P.Xs[i] + 1:D2}{P.Ys[i] + 1:D2}").GetComponent<Button>();
			if (obj.image.color == ButtonClick.red) obj.image.color = ButtonClick.magenta;
			if (obj.image.color == ButtonClick.blue) obj.image.color = ButtonClick.cyan;

			// カレントプレイヤーのテキストを白くする
			if (i == Pc4)
				obj.GetComponentInChildren<Text>().color = Color.white;
		}
	}// end of PaintToLocation

	/// プレイヤーの移動情報を表示する
	/// @ param
	///		it    : Image, Textのコンポーネント
	///		t     : 移動候補の添え字
	///		first : 被りを判断するかどうか
	public static void ShowMoveInfo(UIs it, int t, bool first)
	{
		// 画像のセット
		var tex = Resources.Load("DirectionImage/" + eachRoute[t]) as Texture2D;
		it.Img.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);

		// 移動方向のセット
		it.Txt.text = null;
		for (int i = 0; i < 3; i++)
			it.Txt.text += $"{i + 1}: {toJp[eachRoute[i + t]]}\n";

		// 1手目に同じマスを候補にしているかどうか
		if (first)
		{
			blinked = false;
			// それぞれの候補の重複を消す
			var over = overlap.FindAll(X => X[1] == "1").Select(X => X[0]).Distinct().ToList();
			var over2 = overlap.FindAll(X => X[1] == "2").Select(X => X[0]).Distinct().ToList();
			new Action(() =>
			{
				foreach (var i in over)
					foreach (var j in over2)
						if (i == j) // 同じマスがあるなら
						{
							blinked = true;
							return;  // 多重ループを抜ける
						}
			})();
		}
	}// end of ShowMoveInfo

	/// 0.7秒毎に点滅させる
	IEnumerator Blink()
	{
		while (blinked)
		{
			im.raw.gameObject.SetActive(visibled = !visibled);
			im.raw2.gameObject.SetActive(visibled);
			yield return new WaitForSeconds(0.7f);
		}
		// 点滅オブジェクトが残っているなら
		if (!blinked && im.raw.gameObject.activeSelf)
		{
			im.raw.gameObject.SetActive(false);
			im.raw2.gameObject.SetActive(false);
		}
	}// end of Blink

	private void OpenBackUp()
	{
		OpenFile();
		try
		{
			using (var _back = new StreamReader(backUpPath))
			{
				string[] _cut = _back.ReadToEnd().Split(',');
				var _list = new List<string>();
				foreach (var row in _cut) _list.Add(row);
				_list.RemoveAll(X => X == "");

				var _ar = _list.ConvertAll(X => int.Parse(X)).ToArray();
				int _it = 0;

				myPx1 = _ar[_it++]; myPy1 = _ar[_it++];
				myPx2 = _ar[_it++]; myPy2 = _ar[_it++];
				enPx1 = _ar[_it++]; enPy1 = _ar[_it++];
				enPx2 = _ar[_it++]; enPy2 = _ar[_it++];

				for (int i = 0; i < hei; i++)
					for (int j = 0; j < wid; j++)
						Buttons.Add($"Button{i + 1:D2}{j + 1:D2}", _ar[_it++]);

				ButtonClick.SetPlayerCount(_ar[_it++]);
				turnCount = _ar[_it++];

				for (int i = 0; i < 4; i++)
					OperatePlayer.Enabled[i] = (_ar[_it++] == 0) ? true : false;
			}
		}
		catch
		{
			estr = "ごめんなさい, 実装できてないみたいです, 許してください\n（\"ファイルを開く\"からお願いします）";
			mess.gameObject.SetActive(true);    // オブジェクトを active に
		}
	}

	/// ファイルデータの読み取り
	private void OpenFile()
	{
		try
		{
			var ss = Application.dataPath;
			var p = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
			string path =
				(p.Contains("Kitakaze")) ? @"C:\Users\Kitakaze\Downloads\" :
				(p.Contains("kazuya")) ? @"C:\Users\kazuya\Downloads\" :
											@"C:\Users\Student\Downloads\";
			var fname = "data.csv";

			using (var sr = new StreamReader(path + fname))
			{
				string[] cut = sr.ReadToEnd().Split(' ', ':');  // ' ', ':'で区切る
				var list = new List<string>();

				foreach (var row in cut) list.Add(row);
				list.RemoveAll(x => x == "");   // Remove null

				var ar = list.ConvertAll(x => int.Parse(x)).ToArray();  // string -> int[]

				int it = 0; // 走査
							//hei = ar[it++]; // height, width
							//wid = ar[it++];
				hei = ar[it++];
				wid = ar[it++];
				//Extensions.Swap(ref hei, ref wid);
				value = new int[hei, wid];

				// コピー
				for (int i = 0; i < hei; i++)
					for (int j = 0; j < wid; j++)
						value[i, j] = ar[it++];

				Extensions.Swap(ref hei, ref wid);
				cpy = new int[hei, wid];
				int ite = 0;
				
				// コピー後、横のマスを作って再代入
				for (int i = 0; i < hei; i++)
					for (int j = 0; j < wid; j++, ite++)
						cpy[i, j] = value[wid - 1 - j, i];

				// エージェントの位置を取得
				myPy1 = ar[it++] - 1;
				myPx2 = ar[it++] - 1;
				myPy2 = ar[it++] - 1;
				myPx1 = ar[it++] - 1;

				// 1人目が左側になるように
				if (myPy1 > myPy2)
				{
					Extensions.Swap(ref myPx1, ref myPx2);
					Extensions.Swap(ref myPy1, ref myPy2);
				}
				// 敵エージェントの位置を設定
				enPx1 = myPx2;
				enPx2 = myPx1;
				enPy1 = myPy1;
				enPy2 = myPy2;

				OutputFile();   // 各データの書き込み
			}
		}
		catch (Exception e)
		{
			estr = e.Message; // エラーメッセージ
			mess.gameObject.SetActive(true);    // オブジェクトを active に
		}
	}// end of OpenFile

	/// 各データのCSVファイルを出力する
	private void OutputFile()
	{
		try
		{
			string ss = Application.dataPath;
			// マスのサイズ
			using (var size = new StreamWriter(ss + "/csv/Size.csv", false))
				size.Write($"{hei},{wid}");

			// 得点
			using (var score = new StreamWriter(ss + "/csv/Score.csv", false))
				foreach (var i in value) score.Write(i + ",");

			// エージェントの位置
			int[,] pos = { { myPx1, myPy1 }, { myPx2, myPy2 }, { enPx1, enPy1 }, { enPx2, enPy2 } };
			using (var agent = new StreamWriter(ss + "/csv/Agent.csv", false))
				foreach (var i in pos) agent.Write(i + ",");
		}
		catch { }
	}// end of OutputFile

	/// ボードの初期化
	private void Initialization()
	{
		firstWeight = new double[hei, wid];
		secondWeight = new double[hei, wid];
		board = new bool[hei, wid];
		player = new int[hei, wid];
		
		// 英語に対応する日本語
		string[] h = { "R", "C", "L" }, v = { "top", "middle", "bottom" },
					_h = { "右 ", "中 ", "左 " }, _v = { " 上", " 中", " 下" };

		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				toJp.Add($"{h[i]}-{v[j]}", $"{_h[i]}-{_v[j]}");

		// オブジェクトの管理
		it1 = new UIs(GameObject.Find("Image1").GetComponent<Image>(), directionText);
		it2 = new UIs(GameObject.Find("Image2").GetComponent<Image>(), directionText2);
		txts = new UIs(allyText, enemyText, turnText, searchText, searchText2, currentPlayerText, warningText);
		raws = new UIs(GameObject.Find("Mask").GetComponent<RawImage>(), GameObject.Find("Mask-2").GetComponent<RawImage>());
		im = new UIs(GameObject.Find("Blink1").GetComponent<RawImage>(), GameObject.Find("Blink2").GetComponent<RawImage>());

		txts.Warn.gameObject.SetActive(false);
		raws.raw.gameObject.SetActive(false);
		im.raw.gameObject.SetActive(false);
		im.raw2.gameObject.SetActive(false);

		// 赤色青色に対応するため
		GameObject.Find("AllyColor").GetComponent<RawImage>().color = ButtonClick.magenta;
		GameObject.Find("EmemyColor").GetComponent<RawImage>().color = ButtonClick.cyan;

		dialog = GameObject.Find("ConfirmDialog-Canvas").GetComponent<Canvas>();
		dialog.gameObject.SetActive(false);
	}// end of Initialization

	/// 各チームの得点を計算する
	/// @ param
	///		Pnum : 取得したマスを判断する値
	public static int Cal(int Pnum)
	{
		// 返り値
		int ret = 0;
		// 囲われている場所を探す
		for (int i = 0; i < hei; i++)
		{
			for (int j = 0; j < wid; j++)
				if (i == 0 || i == hei - 1 || j == 0 || j == wid - 1)
					Score(i, j, Pnum);
		}
		// 得点計算
		for (int i = 0; i < hei; i++)
		{
			for (int j = 0; j < wid; j++)
			{
				// 通っているなら
				if (board[i, j] && player[i, j] == Pnum)
					ret += cpy[i, j];
				// 囲んでいるなら
				else if (board[i, j])
					ret += (cpy[i, j] > 0 ? cpy[i, j] : cpy[i, j] * -1);
			}
		}
		return ret;
	}// end of Cal

	/// 各チームの得点
	/// @ param
	///		i    : 縦軸
	///		j    : 横軸
	///		Pnum : 取得したマスを判断する値
	private static void Score(int i, int j, int Pnum)
	{
		// 移動用配列
		int[] vx = { 1, 0, -1, 0 }, vy = { 0, 1, 0, -1 };
		// 初期化
		if (i == 0 && j == 0)
		{
			for (int x = 0; x < hei; x++)
				for (int y = 0; y < wid; y++)
					board[x, y] = true;
		}

		// 探索中の場所が自分のタイルでなかったら無効
		if (player[i, j] != Pnum) board[i, j] = false;
		else return;

		// 囲われている場所を探す
		for (int x = 0; x < 4; x++)
		{
			if (0 <= i + vx[x] && i + vx[x] < hei && 0 <= j + vy[x] && j + vy[x] < wid)
				if (player[i + vx[x], j + vy[x]] != Pnum && board[i + vx[x], j + vy[x]])
					// 移動して再帰
					Score(i + vx[x], j + vy[x], Pnum);
		}
	}// end of Score

	/// Playerから3マス先までの重みを付ける
	/// @ param
	///		x, y  : 現在地
	///		Board : マスの得点
	/// @ memo
	///		1マス先 - * 1.0
	///		2マス先 - * 0.7
	///		3マス先 - * 0.4
	private static void GiveWeight(int x, int y, ref double[,] board)
	{
		Array.Copy(value, board, value.Length);
		double[] attach = { 0, 1.0, 0.7, 0.4 };   // 重み倍率
		int ah = 0;    // 中心からahマス先

		// 重みづけ
		while (++ah <= 3)
		{
			for (int i = -ah; i < ah; i++)
			{
				try { board[x + i, y - ah] = value[x + i, y - ah] * attach[ah]; } catch { }
				try { board[x + i + 1, y + ah] = value[x + i + 1, y + ah] * attach[ah]; } catch { }
				try { board[x + ah, y + i] = value[x + ah, y + i] * attach[ah]; } catch { }
				try { board[x - ah, y + i + 1] = value[x - ah, y + i + 1] * attach[ah]; } catch { }
			}
		}
	}// end of GiveWeight

	/// 取得マスの定義
	/// @ memo
	///		Ally  :  1
	///		None  :  0
	///		Enemy : -1
	private struct PlayerColor
	{
		public static int A = 1,    // 味方のマス (Ally
						  N = 0,    // 誰のマスでもない (None
						  E = -1;   // 敵のマス (Enemy
	}// end of PlayerColor

	/// ルート探索
	private class SearchRoute
	{
		public static int[,,] route = new int[729, 3, 2];   // ルートを記憶
		public static double[] score = new double[729];  // 得点を記憶

		/// 3手先までのルートを求める
		public static void Search()
		{
			// 要素をリセットする
			overlap.Clear();
			eachRoute.Clear();
			txts.Sach.text = txts.Sach2.text = null;

			Array.Clear(route, 0, route.Length);
			Array.Clear(score, 0, score.Length);

			// First
			FindRoute(ref firstWeight, myPx1, myPy1);
			ShowRoute(1, myPx1, myPy1, ref txts.Sach);

			// 1人目のルート候補数を記憶
			candidateNum = eachRoute.Count;

			// Second
			FindRoute(ref secondWeight, myPx2, myPy2);
			ShowRoute(2, myPx2, myPy2, ref txts.Sach2);

		}// end of Search

		/// ルートを表示する
		/// @ param
		///		times  : プレイヤー番号
		///		Px, Py : 現在地
		private static void ShowRoute(int times, int Px, int Py, ref Text t)
		{
			var prefab = GameObject.Find("RouteExchangeButton");    // get button inf
			var edge = prefab.GetComponent<RectTransform>().sizeDelta.y;   // ボタン一辺の長さ
			var yAxis = -53.2f;  // 初期位置

			var act = "";     // 行動パターン
			int an, bn, nx, ny;

			int n = GetLoopTimes(Px, Py); // 繰り返し回数
			for (int i = 0; i < n; i++)
			{
				nx = Px + 'A'; ny = Py + 1;
				for (int j = 0; j < route.GetLength(1); j++)
				{
					an = route[i, j, 1]; // 縦軸の方向
					bn = route[i, j, 0]; // 横軸の方向
					t.text += $"{(char)(nx += bn)}{(ny += an),-2} ";

					// Route string
					act = (an == 1) ? "R-" : (an == 0) ? "C-" : "L-";
					act += (bn == 1) ? "bottom" : (bn == 0) ? "middle" : "top";

					if (j == 0)   // 一手先のみ
						overlap.Add(new string[] { $"{(char)nx}{ny}", $"{times}" });
					eachRoute.Add(act);
				}
				t.text += $"-> {score[i]}\n";

				var b = Instantiate(prefab, new Vector2(20, yAxis), Quaternion.identity);
				b.tag = "RouteNumber";
				b.GetComponentInChildren<Text>().text = $"{i + 1}";

				if (times == 1)     // 一人目なら
				{
					b.name = $"oneNum{i}";
					b.transform.SetParent(GameObject.Find("Panel").transform, false);
				}
				else
				{
					b.name = $"twoNum{i}";
					b.transform.SetParent(GameObject.Find("Panel-2").transform, false);
				}
				yAxis -= edge;  // y軸をずらす
			}
		}// end of ShowRoute

		/// ルート候補数を調べる
		/// @ param
		///		Px, Py : 現在地
		private static int GetLoopTimes(int Px, int Py)
		{
			int r_n = 0;        // 候補数を数える
			try
			{
				// 昇順ソート
				int cnt = 0;    // 得点が３番目に大きいルートまでループするため
				for (r_n = 0; cnt < 3; r_n++)
				{
					for (int j = score.Length - 1; j > r_n; j--)
					{
						if (score[j] > score[j - 1])
						{
							// 得点入れ替え
							var tempS = score[j];
							score[j] = score[j - 1];
							score[j - 1] = tempS;

							// ルート入れ替え
							for (int k = 0; k < route.GetLength(1); k++)
							{
								for (int l = 0; l < route.GetLength(2); l++)
								{
									var tempR = route[j, k, l];
									route[j, k, l] = route[j - 1, k, l];
									route[j - 1, k, l] = tempR;
								}
							}
						}
					}

					// 得点の候補数を数える
					cnt = 1;
					var rS = score[0];
					for (int j = 1; j <= r_n; j++)
					{
						if (rS != score[j])
						{
							cnt++;
							rS = score[j];
						}
						if (cnt == 1)
						{
							for (int k = r_n; k > j - 1; k--)
							{
								//一手目の得点が一番高いルートを上に持ってくる
								if (value[Px + route[k, 0, 0], Py + route[k, 0, 1]] > value[Px + route[k - 1, 0, 0], Py + route[k - 1, 0, 1]])
								{
									// 得点入れ替え
									var tempS = score[k];
									score[k] = score[k - 1];
									score[k - 1] = tempS;

									// ルート入れ替え
									for (int l = 0; l < route.GetLength(1); l++)
									{
										for (int m = 0; m < route.GetLength(2); m++)
										{
											var tempR = route[k, l, m];
											route[k, l, m] = route[k - 1, l, m];
											route[k - 1, l, m] = tempR;
										}
									}
								}
							}
						}
					}
				}
			}
			catch { }
			return r_n;
		}// end of GetLoopTimes

		/// ルートの候補を見つける
		/// @ param
		///		Field  : マスの得点
		///		Px, Py : 現在地
		public static void FindRoute(ref double[,] Field, int Px, int Py)
		{
			// i, k, m -> Vertical // j, l, n -> Horizontal

			GiveWeight(Px, Py, ref Field);   // 重みづけ

			int RouteCnt = 0;   //ルート数
			int oneM, twoM, thiM,
				 x, y;

			// Search
			for (int i = -1; i <= 1; i++)
			{
				for (int j = -1; j <= 1; j++)
				{
					x = Px + i;
					y = Py + j;

					// 配列外なら
					if (x < 0 || x > hei - 1 || y < 0 || y > wid - 1) continue;

					oneM = player[x, y]; // 1マス先を記憶

					// 1マス先 -> 味方のマス
					if (oneM == PlayerColor.A) continue;

					// 1マス先 -> 誰のマスでもない
					else if (oneM == PlayerColor.N)
					{
						player[x, y] = PlayerColor.A; // マスを通る
						for (int k = -1; k <= 1; k++)
						{
							for (int l = -1; l <= 1; l++)
							{
								// 配列外なら
								if (x + k < 0 || x + k > hei - 1 || y + l < 0 || y + l > wid - 1) continue;

								twoM = player[x + k, y + l];  // 2マス先を記憶

								// 2マス先 -> 自分のマス
								if (twoM == PlayerColor.A) continue;

								// 2マス先 -> 誰のマスでもない
								else if (twoM == PlayerColor.N)
								{
									player[x + k, y + l] = PlayerColor.A;  // マスを通る

									for (int m = -1; m <= 1; m++)
									{
										for (int n = -1; n <= 1; n++)
										{
											// 配列外なら
											if (x + k + m < 0 || x + k + m > hei - 1 || y + l + n < 0 || y + l + n > wid - 1) continue;

											thiM = player[x + k + m, y + l + n];   // 3マス先を記憶

											// 3マス先 -> 自分のマス
											if (thiM == PlayerColor.A) continue;

											// 3マス先 -> 誰のマスでもない
											else if (thiM == PlayerColor.N)
											{
												// ルートを記憶
												RouteIn(i, j, k, l, m, n, RouteCnt);
												// 合計値
												score[RouteCnt] = Field[x, y] + Field[x + k, y + l] + Field[x + k + m, y + l + n];
												RouteCnt++;
											}

											// 3マス先 -> 敵のマス
											else if (thiM == PlayerColor.E)
											{
												RouteIn(i, j, k, l, m, n, RouteCnt);
												score[RouteCnt] = Field[x, y] + Field[x + k, y + l];
												RouteCnt++;
											}
										}
									}
								}
								// 2マス先 -> 敵のマス
								else if (twoM == PlayerColor.E)
								{
									RouteIn(i, j, k, l, 0, 0, RouteCnt);
									score[RouteCnt] = Field[x, y] + Field[x + k, y + l];
									RouteCnt++;
								}
								player[x + k, y + l] = twoM;  // マスの色を戻す
							}
						}
					}
					// 1マス先 -> 敵のマス
					else if (oneM == PlayerColor.E)
					{
						player[x, y] = PlayerColor.A; // マスを通る
						for (int k = -1; k <= 1; k++)
						{
							for (int l = -1; l <= 1; l++)
							{
								// 配列外なら
								if (x + k < 0 || x + k > hei - 1 || y + l < 0 || y + l > wid - 1) continue;

								thiM = player[x + k, y + l];    // 3マス先を記憶

								// 3マス先 -> 味方のマス
								if (thiM == PlayerColor.A) continue;

								// 3マス先 -> 誰のマスでもない
								else if (thiM == PlayerColor.N)
								{
									RouteIn(i, j, 0, 0, k, l, RouteCnt);
									score[RouteCnt] = Field[x, y] + Field[x + k, y + l];
									RouteCnt++;
								}
								// 3マス先 -> 敵のマス
								else if (thiM == PlayerColor.E)
								{
									RouteIn(i, j, 0, 0, k, l, RouteCnt);
									score[RouteCnt] = Field[x, y];
									RouteCnt++;
								}
							}
						}
					}
					player[x, y] = oneM;    // マスの色を戻す
				}
			}
		}// end of FindRoute

		/// ルート記憶
		/// @ param
		///		x1, y1  : 1手先
		///		x2, y2  : 2手先
		///		x3, y3  : 3手先
		///		cnt     : 添え字
		private static void RouteIn(int x1, int y1, int x2, int y2, int x3, int y3, int cnt)
		{
			route[cnt, 0, 0] = x1; route[cnt, 0, 1] = y1;
			route[cnt, 1, 0] = x2; route[cnt, 1, 1] = y2;
			route[cnt, 2, 0] = x3; route[cnt, 2, 1] = y3;
		}// end of RouteIn
	}// end of SearchRoute

}// end of SystemProgram
