﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static SystemProgram;

/// UnityEngine.UI Component
public class UIs
{
	public Text Ally, Enemy, Turn, Sach, Sach2, Cur, Warn;
	public RawImage raw, raw2;

	public Image Img;
	public Text Txt;

	// for Text
	public UIs(Text a, Text e, Text t, Text s, Text s2, Text c, Text w)
	{ Ally = a; Enemy = e; Turn = t; Sach = s; Sach2 = s2; Cur = c; Warn = w; }

	// for RawImage
	public UIs(RawImage r, RawImage r2) { raw = r; raw2 = r2; }

	// for Image, Text
	public UIs(Image i, Text t) { Img = i; Txt = t; }
}// end of UIs


public class NewFunc : MonoBehaviour
{
	private GameObject panel;       // Board
	private GameObject[] caps;      // Caption

	private static int rotate = 0,  // 回転値
						reverse = 0;

	void Start()
	{
		// Data Initialization
		panel = GameObject.Find("Panel_Board");
		caps = GameObject.FindGameObjectsWithTag("Caption");
	}// end of Start

	/// ScoreBoardを作成する
	/// @ param
	///		loc : プレイヤー位置
	public static void CreateButton()
	{
		var prefabB = (GameObject)Resources.Load("prefabs/BoardButton");    // プレハブ取得
		var edge = prefabB.GetComponent<RectTransform>().sizeDelta.x;       // 長さ取得

		// Panel Size
		var panel = GameObject.Find("Panel_Board");
		panel.GetComponent<RectTransform>().sizeDelta = new Vector2(edge * wid + 80, edge * hei + 80);  // + 100 -> 余白
		panel.GetComponent<RectTransform>().localPosition = new Vector2(-455f, -30);   // x's value -> Panel center

		var vc = new Vector2(60, -60);
		var P = ButtonClick.GetAllLocation();

		for (int i = 0; i < hei; i++)
		{
			for (int j = 0; j < wid; j++)
			{
				// インスタンス生成
				var b = Instantiate(prefabB, new Vector2(vc.x + (j * edge), vc.y), Quaternion.identity);
				b.name = $"Button{i + 1:D2}{j + 1:D2}";
				b.tag = "ScoreBoard";
				b.transform.SetParent(panel.transform, false);
				b.GetComponentInChildren<Text>().text = $"{cpy[i, j]}";

				// バックアップから出ない場合
				if (!StartUp.backed)
				{
					// プレイヤーの初期位置なら
					if (i == P.Xs[0] && j == P.Ys[0] || i == P.Xs[1] && j == P.Ys[1])
					{
						// データを付与 (みかた
						b.GetComponent<Button>().image.color = ButtonClick.red;
						Buttons[$"Button{i + 1:D2}{j + 1:D2}"] = 1;
					}
					//if (i == P.Xs[2] && j == P.Ys[2] || i == P.Xs[3] && j == P.Ys[3])
					//{
					//	// データを付与 (てき
					//	b.GetComponent<Button>().image.color = ButtonClick.blue;
					//	Buttons[$"Button{i + 1:D2}{j + 1:D2}"] = -1;
					//}
				}
			}
			vc.y -= edge;   // y軸をずらす
		}

		// キャプション
		var verCap = (GameObject)Resources.Load("prefabs/Vertical-textCaption");
		var horCap = (GameObject)Resources.Load("prefabs/Horizontal-textCaption");
		var _v = new Vector2(22, -60);
		var _h = new Vector2(60, -22);

		// 縦軸のキャプション
		for (int i = 0; i < hei; i++)
		{
			var vCap = Instantiate(verCap, new Vector2(_v.x, _v.y), Quaternion.identity);
			vCap.name = $"Verticals{i}";
			vCap.transform.SetParent(panel.transform, false);
			vCap.GetComponentInChildren<Text>().text = $"{(char)('A' + i)}";
			_v.y -= edge;
		}
		// 横軸のキャプション
		for (int j = 0; j < wid; j++)
		{
			var hCap = Instantiate(horCap, new Vector2(_h.x, _h.y), Quaternion.identity);
			hCap.name = $"Horizontal{j}";
			hCap.transform.SetParent(panel.transform, false);
			hCap.GetComponentInChildren<Text>().text = $"{j + 1}";
			_h.x += edge;
		}
	}// end of CreateButton

	/// ボードを90°回転する
	public void RotatePanel()
	{
		rotate = (rotate + 90) % 360;
		panel.transform.rotation = Quaternion.Euler(0, reverse, -rotate);

		ResetRotation();
	}// end of RotatePanel

	/// ボードを水平反転する
	public void ReversePanel()
	{
		reverse = (reverse + 180) % 360;
		panel.transform.rotation = Quaternion.Euler(0, reverse, rotate);

		ResetRotation();
	}// end of ReversePanel

	/// Panelの子要素の回転をリセットする
	public void ResetRotation()
	{
		// 子要素の取得
		var child = GameObject.FindGameObjectsWithTag("ScoreBoard");

		foreach (var obj in child)
			obj.transform.rotation = Quaternion.Euler(0, 0, 0);
		foreach (var obj in caps)
			obj.transform.rotation = Quaternion.Euler(0, 0, 0);
	}// end of ResetRotation

	/// 履歴管理
	public static class MyStack
	{
		const int MAX_CAPACITY = 10;    // 履歴上限数

		/*-- for ButtonClick --*/
		/// 履歴を追加する
		/// @ param
		///		item : 履歴
		public static void PushLog(History item, ref LinkedList<History> list)
		{
			list.AddFirst(item);
			if (list.Count > MAX_CAPACITY)   // MAX_CAPACITYを超えたら
				list.RemoveLast();           // 一番下の要素を消す
		}// end of PushLog

		/*-- for OperatePlayer --*/
		/// 履歴を追加する
		/// @ param
		///		item : 履歴
		public static void PushLog(int item, ref LinkedList<int> list)
		{
			list.AddFirst(item);
			if (list.Count > MAX_CAPACITY)  // MAX_CAPACITYを超えたら
				list.RemoveLast();         // 一番下の要素を消す
		}// end of PushLog

		/// 履歴を取り出す
		public static T PopLog<T>(ref LinkedList<T> list)
		{
			var item = list.First();
			list.RemoveFirst();
			return item;
		}// end of PopLog

	}// end of MyStack

}// end of NewFunc
