﻿using UnityEngine;
using UnityEngine.UI;

public class PanelScroll : MonoBehaviour
{
	private Scrollbar sclbar;  // スクロールバー
	private GameObject panel, panel2;   // パネル
	private Vector2 pos, pos2;          // パネルの位置
	private const int SCROLL_VALUE = 9000;	// スクロール値

	void Start()
	{
		// get component
		sclbar = gameObject.GetComponent<Scrollbar>();
		sclbar.value = 0;   // set Value

		panel = GameObject.Find("Panel");
		pos = panel.transform.localPosition;

		panel2 = GameObject.Find("Panel-2");
		pos2 = panel2.transform.localPosition;
	}// end of Start

	/// スクロールバーを動かしたとき
	///		value に応じて panel を移動させる
	public void OnChanged() =>
		panel.transform.localPosition = new Vector2(pos.x, pos.y + sclbar.value * SCROLL_VALUE);
	// end of OnChanged

	public void OnChange2() =>
		panel2.transform.localPosition = new Vector2(pos2.x, pos2.y + sclbar.value * SCROLL_VALUE);
	// end of OnChanged2

}// end of PanelScroll
