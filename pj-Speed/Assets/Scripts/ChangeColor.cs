﻿using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.Color;

public class ChangeColor : MonoBehaviour
{
	private static Button colorBtn, returnBtn;  // ボタン
	private static Color color;     // 変更する色
	private static string text;     // テキスト
	public static Image remImg;
	private static string[] txt = new string[] { "Red", "Blue" };
	private static Color[] clr = new Color[] { red, blue };

	public static bool changed = true;      // Red / Blue
	public static bool returned = false;    // is Returned
	public static bool removed = false;


	void Start()
	{
		// Get GameObject
		colorBtn = GameObject.Find("ColorChangeButton").GetComponent<Button>();
		returnBtn = GameObject.Find("ColorReturnButton").GetComponent<Button>();
		remImg = GameObject.Find("RemoveColorBtn").GetComponent<Button>().image;

		if (!StartUp.colors)
		{
			txt[0] = "Blue"; txt[1] = "Red";
			clr[0] = blue; clr[1] = red;
		}
	}// end of Start

	/// ボタンの色を赤 / 青に変える
	public void OnChangeColor()
	{
		changed = !changed; // 反転

		if (changed) { color = clr[0]; text = txt[0]; }
		else { color = clr[1]; text = txt[1]; }
		ToChange(color, colorBtn, text);

		// returnButton -> Off
		returned = false;
		ToChange(black, returnBtn, "OFF");
	}// end of OnChangeColor

	/// ボタンの色をwhiteに戻す
	public void OnReturnColor()
	{
		returned = !returned;   // 反転

		if (returned) { color = yellow; text = "Ret"; }
		else { color = black; text = "OFF"; }
		ToChange(color, returnBtn, text);

		if (!returned) { color = (changed) ? clr[0] : clr[1]; text = (changed) ? txt[0] : txt[1]; }
		else { color = black; text = "OFF"; }
		ToChange(color, colorBtn, text);

	}// end of OnReturnColor

	/// 自分のマスの色を消す
	public void OnRemoveMyColor()
	{
		removed = !removed;
		if (removed) remImg.color = yellow;
		else remImg.color = white;
	}// end of OnRemoveMyColor

	/// ボタンの色を変更する
	/// @ param
	///		clr : 変更後の色
	///		btn : 変更するボタン
	///		txt : textの内容
	public static void ToChange(Color clr, Button btn, string txt)
	{
		// ボタンの色をclrに変更
		btn.image.color = clr;

		var colors = btn.colors;
		colors.normalColor = clr;
		colors.highlightedColor = clr;
		colors.pressedColor = clr;
		colors.disabledColor = clr;

		btn.colors = colors;

		btn.GetComponentInChildren<Text>().text = txt;
	}// end of ToChange

	/// 次のプレイヤーカラーを判断する
	public static void JudgeNextColor()
	{
		// 外に出して一度きりifを実行する
		// 複数個にあるため できなければこのまま
		// 外に出せるならToChangeあたりも簡単？
		switch (SystemProgram.Pc4)
		{
			// A-Team
			case 0:
			case 1: changed = true; ToChange(clr[0], colorBtn, txt[0]); break;
			// B-Team
			case 2:
			case 3: changed = false; ToChange(clr[1], colorBtn, txt[1]); break;
		}
	}// end of DetermineNextColor

}// end of ChangeColor
