﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.Color;
using static SystemProgram;
using static UnityEngine.Debug;
using static NewFunc;

public class History
{
	public string Name;
	public Color Color;
	public int X;
	public int Y;
	public bool Ret;
	public Color TextC;
	public int Xm;
	public int Ym;
	public int NN;

	public History(string name, Color clr, int x, int y, bool ret, Color tClr, int xm, int ym, int nn)
	{ Name = name; Color = clr; X = x; Y = y; Ret = ret; TextC = tClr; Xm = xm; Ym = ym; NN = nn; }
}// end of History

public class Loc
{
	public int X;
	public int Y;
	public int[] Xs = new int[4];
	public int[] Ys = new int[4];

	public Loc(int x, int y) { X = x; Y = y; }
	public Loc(int[] x, int[] y) { Array.Copy(x, Xs, 4); Array.Copy(y, Ys, 4); }
}// end of Pos

public static class Extensions
{
	public static void Swap<T>(ref T x, ref T y)
	{
		T tmp = x;
		x = y;
		y = tmp;
	}
}// end of Extensions

public partial class ButtonClick : MonoBehaviour
{
	private Image btnImg;
	private Text btnTxt;
	public static Color magenta = Color.magenta,
						cyan = Color.cyan,
						red = Color.red,
						blue = Color.blue;

	private static LinkedList<History> logList = new LinkedList<History>(); // 履歴
	private static Dictionary<string, int> copyButtons = new Dictionary<string, int>();
	private static string[] place = new string[4];  // エージェントの位置
	private static Loc all = new Loc(0, 0);  // 
	public static int[] prev = new int[4];  // 変更したボタンの1ターン前


	void Start()
	{
		// Get GameObject
		btnImg = gameObject.GetComponent<Button>().image;
		btnTxt = gameObject.GetComponentInChildren<Text>();
	}// end of Start


	/// 速さ重視、相手のマスを指定しない
	public void OnClick_Speed()
	{
		var array = name.Substring(6).ToArray();
		int x = int.Parse($"{array[0]}{array[1]}") - 1,  // Vertical
			y = int.Parse($"{array[2]}{array[3]}") - 1;  // Horizontal

		// 1マス先かどうか
		var dif = new Loc(0, 0);    // 現在地からクリック先の方向を取得
		if (!IsMovable(x, y, Pc4 * 2, ref dif, myPx1, myPy1, myPx2, myPy2, enPx1, enPy1, enPx2, enPy2)) return;

		// AssistMode なら
		if (Assist.selected) Assist.Assisting(dif.X, dif.Y);

		var pos = GetLocation();
		MyStack.PushLog(new History(name, btnImg.color, pos.X, pos.Y, ChangeColor.returned, btnTxt.color, 0, 0, -1), ref logList);
		MyStack.PushLog(Pc4, ref OperatePlayer.pNumList);

		if (ChangeColor.removed)
		{
			// 色がついたマスのみ
			if (Buttons[name] == 0) return;

			// プレイヤーの現在地(自分の位置も)を選択したら return
			if (place.Any(X => X == name.Substring(6)) || $"{pos.X + 1:D2}{pos.Y + 1:D2}" == name.Substring(6)) return;
			btnImg.color = white;
			Buttons[name] = 0;
		}

		else
		{
			// change -> red
			if (ChangeColor.changed)
			{
				if (btnImg.color == blue || btnImg.color == cyan) { btnImg.color = white; Buttons[name] = 0; }
				else { btnImg.color = red; Buttons[name] = 1; SetLocation(x, y); }
			}
			// !changed -> blue
			else
			{
				if (btnImg.color == red || btnImg.color == magenta) { btnImg.color = white; Buttons[name] = 0; }
				else { btnImg.color = blue; Buttons[name] = -1; SetLocation(x, y); }
			}
		}
		// ProcessAfterMoved のいちぶ
		Round++;
		PlayerCount++;
		Apply();

	}

	//使わない
	/// ボードのボタンがクリックされたら
	public void OnClickButton()
	{
		// returned == true  -> white
		if (ChangeColor.returned)
		{
			var P = GetAllLocation();
			var xx = new string[4];
			for (int i = 0; i < 4; i++)
				xx[i] = $"{P.Xs[i] + 1:D2}{P.Ys[i] + 1:D2}";

			// 現在地なら
			if (xx.Any(X => X == name.Substring(6))) return;

			var loc = GetLocation();
			MyStack.PushLog(new History(name, btnImg.color, loc.X, loc.Y, ChangeColor.returned, btnTxt.color, 0, 0, -1), ref logList);
			btnImg.color = white;
			Buttons[name] = 0;
			Apply();
			return;
		}

		var array = name.Substring(6).ToArray();
		int x = int.Parse($"{array[0]}{array[1]}") - 1,  // Vertical
			y = int.Parse($"{array[2]}{array[3]}") - 1;  // Horizontal

		// 1マス先かどうか
		var dif = new Loc(0, 0);    // 現在地からクリック先の方向を取得
		if (!IsMovable(x, y, Pc4 * 2, ref dif, myPx1, myPy1, myPx2, myPy2, enPx1, enPy1, enPx2, enPy2)) return;

		// AssistMode なら
		if (Assist.selected) Assist.Assisting(dif.X, dif.Y);

		if (Round % 4 == 0)
		{
			all = GetAllLocation();
			copyButtons = new Dictionary<string, int>(Buttons);
		}
		var skiped = false;

		// 他プレイヤーの場所を選択したかどうか
		if (IsCurrentLocation(name.Substring(6)))
		{
			var id = Array.IndexOf(place, name.Substring(6));    // 一致するプレイヤーの添え字
			var team = (id <= 1) ? 1 : -1;  // チームの判断

			var l = GetLocation();
			var co = GetAllLocation();  // 現在の位置

			// 戻す対象なら
			//		&& 自分のチーム以外の時
			if (OperatePlayer.Enabled[id] &&
				$"{all.Xs[id] + 1:D2}{all.Ys[id] + 1:D2}" != name.Substring(6) &&
				Buttons[$"Button{l.X + 1:D2}{l.Y + 1:D2}"] != team)
			{
				MyStack.PushLog(new History(name, btnImg.color, l.X, l.Y, ChangeColor.returned, btnTxt.color, co.Xs[id], co.Ys[id], id
					), ref logList);
				MyStack.PushLog(Pc4, ref OperatePlayer.pNumList);

				switch (id)
				{
					// プレイヤーの位置を1ターン前に戻す
					case 0: myPx1 = all.Xs[id]; myPy1 = all.Ys[id]; break;
					case 1: myPx2 = all.Xs[id]; myPy2 = all.Ys[id]; break;
					case 2: enPx1 = all.Xs[id]; enPy1 = all.Ys[id]; break;
					case 3: enPx2 = all.Xs[id]; enPy2 = all.Ys[id]; break;
				}

				// 1ターン前のマスと現在のマスで違いがあるなら (赤/青/白 を区別するため)
				var pName = $"Button{prev[id]:D4}";
				if (Buttons[pName] != copyButtons[pName])
				{
					// 元のマスを参照して元に戻す
					GameObject.Find(pName).GetComponent<Button>().image.color = (copyButtons[pName] == 1) ? red : (copyButtons[pName] == 0) ? white : blue;
					Buttons[pName] = copyButtons[pName];
				}

				// 戻した先の色
				GameObject.Find($"Button{all.Xs[id] + 1:D2}{all.Ys[id] + 1:D2}").GetComponent<Button>().image.color
					= (team == 1) ? magenta : cyan;
				skiped = true;
			}
			// 戻す対象でないなら
			else return;
		}
		if (!skiped)
		{
			var pos = GetLocation();
			MyStack.PushLog(new History(name, btnImg.color, pos.X, pos.Y, ChangeColor.returned, btnTxt.color, 0, 0, -1), ref logList);
			MyStack.PushLog(Pc4, ref OperatePlayer.pNumList);

			if (ChangeColor.removed)
			{
				// 色がついたマスのみ
				if (Buttons[name] == 0) return;

				// プレイヤーの現在地(自分の位置も)を選択したら return
				if (place.Any(X => X == name.Substring(6)) || $"{pos.X + 1:D2}{pos.Y + 1:D2}" == name.Substring(6)) return;
				btnImg.color = white;
				Buttons[name] = 0;
			}

			else
			{
				// change -> red
				if (ChangeColor.changed)
				{
					if (btnImg.color == blue || btnImg.color == cyan) { btnImg.color = white; Buttons[name] = 0; }
					else { btnImg.color = red; Buttons[name] = 1; SetLocation(x, y); }
				}
				// !changed -> blue
				else
				{
					if (btnImg.color == red || btnImg.color == magenta) { btnImg.color = white; Buttons[name] = 0; }
					else { btnImg.color = blue; Buttons[name] = -1; SetLocation(x, y); }
				}
			}
		}
		prev[Pc4] = (int.Parse(name.Substring(6)));
		ProcessAfterMoved();

	}// end of OnClickButton

	/// 移動した後の処理
	private static void ProcessAfterMoved()
	{
		OperatePlayer.FinishedOperating(Pc4);

		// すべてのプレイヤーを動かしたなら（すべてtrueなら）
		bool enabled = OperatePlayer.Enabled.All(X => X == true);
		if (enabled)
		{
			SetPlayerCount(0);
			turnCount++;
			OperatePlayer.ClearColor();
		}
		// false の添え字を取得
		int index = Array.IndexOf(OperatePlayer.Enabled, false);
		SetPlayerCount(index);    // index と等しく

		ChangeColor.removed = false;
		ChangeColor.remImg.color = white;

		Round++;
		Apply();
	}// end of ProcessAfterMoved

	/// 現在地かどうか
	/// @ param
	///		name : ボタンの番号
	private static bool IsCurrentLocation(string name)
	{
		var others = new string[4];
		var P = GetAllLocation();
		for (int i = 0; i < 4; i++)
		{
			// プレイヤーの位置を記憶
			if (i == Pc4) others[i] = null;  // 動かないとき
			else others[i] = $"{P.Xs[i] + 1:D2}{P.Ys[i] + 1:D2}";
		}
		Array.Copy(others, place, others.Length);
		// プレイヤーがいるなら true
		return others.Any(X => X == name);
	}// end of IsCurrentLocation

	/// 移動可能かどうか
	/// @ param
	///		x,y : クリックした位置
	///		t   : プレイヤー番号
	///		L   : 現在地からクリックした場所の差
	///		P   : 各プレイヤー位置
	private static bool IsMovable(int x, int y, int t, ref Loc L, params int[] P)
		// 絶対値が1以下なら true
		=> (Mathf.Abs(L.X = x - P[t]) <= 1 && Mathf.Abs(L.Y = y - P[t + 1]) <= 1);
	// end of IsMovable

	/// 一回前の状態に戻す
	public void OnUndo()
	{
		if (logList.Count == 0) return;    // 中身がない場合は return
		var first = OperatePlayer.pNumList.First();
		var undo = MyStack.PopLog(ref logList);

		if (undo == null)
		{
			if (--Round % 4 == 3)       // 1巡目なら
				turnCount--;
			OperatePlayer.WhenCalledOnUndo();

			SetPlayerCount(first);
			Apply();
			return;
		}

		if (undo.Ret)
		{
			GameObject.Find(undo.Name).GetComponent<Button>().image.color = undo.Color;
			Apply();
			return;
		}

		GameObject.Find(undo.Name).GetComponent<Button>().image.color = undo.Color;

		// SystemProgram.Buttonsを書き換える
		Buttons[undo.Name]     // obj.color
			= (undo.Color == magenta || undo.Color == red) ? 1  // == red  ->  1
			: (undo.Color == cyan || undo.Color == blue) ? -1   // == blue -> -1
			: 0;    // else    ->  0

		if (--Round % 4 == 3)
			turnCount--;
		OperatePlayer.WhenCalledOnUndo();

		SetPlayerCount(first);

		if (undo.NN == -1)
			SetLocation(undo.X, undo.Y);

		else    // 指定先が被った場合, 位置を戻す
		{
			switch (undo.NN)
			{
				case 0: myPx1 = undo.Xm; myPy1 = undo.Ym; break;
				case 1: myPx2 = undo.Xm; myPy2 = undo.Ym; break;
				case 2: enPx1 = undo.Xm; enPy1 = undo.Ym; break;
				case 3: enPx2 = undo.Xm; enPy2 = undo.Ym; break;
			}
		}

		btnTxt = GameObject.Find(undo.Name).GetComponentInChildren<Text>();
		btnTxt.color = undo.TextC;
		Apply();
	}// end of OnUndo

	/// パスする
	///		何もしない / 無効（意思表示失敗）
	public void OnPass()
	{
		// push
		MyStack.PushLog(null, ref logList);
		MyStack.PushLog(Pc4, ref OperatePlayer.pNumList);
		
		Round++;
		PlayerCount++;
		Apply();
	}// end of OnPass

	/// ボタンを押されたときルートを変更する
	public void OnChangeRoute()
	{
		// 1人目か2人目かの判断
		var either = gameObject.name.Contains("one");
		var num = int.Parse(name.Substring(6));

		if (either) ShowMoveInfo(it1, num * 3, false);
		if (!either) ShowMoveInfo(it2, candidateNum + (num * 3), false);
	}// end of OnChangeRoute

	/// 確認ダイアログ画面を表示する
	public void OnConfirmDialog()
		=> dialog.gameObject.SetActive(true);

	/// アプリを終了する
	///		盤面の取得状況 / 得点をファイルに書き込む
	public void OnYesDialog()
	{
		using (var writer = new System.IO.StreamWriter($"{Application.dataPath}/Result.txt", true))
		{
			writer.WriteLine($"{turnCount - 1} - Turn(No.{Pc4 + 1}) // {Cal(1)} : {Cal(-1)}");
			for (int i = 0; i < hei; i++)
			{
				for (int j = 0; j < wid; j++)
					writer.Write(player[i, j] == 1 ? " ●" : player[i, j] == -1 ? " ○" : " －");
				writer.WriteLine();
			}
			writer.WriteLine("**" + DateTime.Now);
			writer.WriteLine();
		}
		Application.Quit();
	}//end of OnYesDialog

	/// Dialogを消して盤面に戻る
	public void OnNoDialog()
		=> dialog.gameObject.SetActive(false);
	// end of OnNoDialog

	/// 現在動かしているプレイヤーの番号を設定する
	public static void SetCurrent()
	{
		var loc = GetLocation();
		txts.Cur.text = $"No.{Pc4 + 1}Player({(char)(loc.X + 'A')}{loc.Y + 1})";
	}// end of PlayerCurrentLocation

	/// プレイヤーのロケーションを設定する
	/// @ param
	///		x : 縦軸
	///		y : 横軸
	private void SetLocation(int x, int y)
	{
		switch (Pc4)
		{
			case 0: myPx1 = x; myPy1 = y; break;
			case 1: myPx2 = x; myPy2 = y; break;
			case 2: enPx1 = x; enPy1 = y; break;
			case 3: enPx2 = x; enPy2 = y; break;
		}
	}// end of SetLocation

	/// プレイヤーのロケーションを取得する
	public static Loc GetLocation()
	{
		int x = 0, y = 0;
		switch (Pc4)
		{
			case 0: x = myPx1; y = myPy1; break;
			case 1: x = myPx2; y = myPy2; break;
			case 2: x = enPx1; y = enPy1; break;
			case 3: x = enPx2; y = enPy2; break;
		}
		return new Loc(x, y);
	}// end of GetLocation

	/// 全プレイヤーの位置を取得する
	public static Loc GetAllLocation()
	{
		int[] xs = new int[4], ys = new int[4];
		for (int i = 0; i < 4; i++)
		{
			switch (i)
			{
				case 0: xs[i] = myPx1; ys[i] = myPy1; break;
				case 1: xs[i] = myPx2; ys[i] = myPy2; break;
				case 2: xs[i] = enPx1; ys[i] = enPy1; break;
				case 3: xs[i] = enPx2; ys[i] = enPy2; break;
			}
		}
		return new Loc(xs, ys);
	}// end of GetAllLocation

	/// PlayerCountを nに合わせる
	/// @ param
	///		n : PlayerCountと等しくするまでの値
	public static void SetPlayerCount(int n) { while (++PlayerCount % 4 != n) ; }

}// end of ButtonClick
